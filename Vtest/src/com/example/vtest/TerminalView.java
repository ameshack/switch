package com.example.vtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.tepi.filtertable.FilterTable;
import org.vaadin.haijian.ExcelExporter;
import org.vaadin.haijian.PdfExporter;

import com.example.model.TerminalModel;
import com.example.model.TransactionLogModel;
import com.example.model.dbsettings;
import com.example.model.logins;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutListener;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomTable.RowHeaderMode;
import com.vaadin.ui.themes.Reindeer;

import de.essendi.vaadin.ui.component.numberfield.NumberField;

public class TerminalView extends VerticalLayout implements View
{
	Navigator navigator1;
	 PdfExporter pdfExporter;
	 ExcelExporter excelExporter;
	 Table terminaltable ;
	 IndexedContainer ic;
	 DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	 public VerticalLayout TerminalView1 ()
	 {
		   //navigator1=navigator;
		// addComponent(new header().myheader(navigator1));
	        
	        Panel Terminalpanel = new Panel("Terminals");
	        Terminalpanel.setSizeFull();
	        
	        final VerticalLayout Terminallayout = new VerticalLayout();
	        Terminallayout.setCaption("Terminals");
	        Terminallayout.setMargin(true);
	        
	        Terminallayout.setSpacing(true);
	        Terminallayout.addStyleName("toolbar");
	        
	        Label title = new Label("Terminals");
	        title.addStyleName("h1");
	        title.setSizeUndefined();
	        Terminallayout.addComponent(title);
	        Terminallayout.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
	 
	    
	       
	        
			Button Terminalbutton = new Button("Add Terminal");
			Terminalbutton.addStyleName("default");

			Terminalbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					UI.getCurrent().addWindow(addTerminal());
					
						//	Notification.show("we want to add a user");
						
				
					
				}
			});
			
			
			terminaltable=TerminalTable();
			Terminallayout.addComponent(Terminalbutton);
			Terminallayout.addComponent(dataExporter());
			
			
			
			
			Terminallayout.addComponent(terminaltable);
			//Terminallayout.addComponent(terminaltable.createControls());
			
		
			//Terminalpanel.setContent(Terminallayout);
			//addComponent(Terminalpanel);
			return Terminallayout;
	        
	 }

	 
	 public HorizontalLayout dataExporter()
	 {
		 HorizontalLayout Exporterlayout = new HorizontalLayout();
		// Exporterlayout.setMargin(true);
		// Exporterlayout.setSpacing(true);
		 Exporterlayout.setWidth("100%");
		 
		 final TextField filter = new TextField();
	        filter.addTextChangeListener(new TextChangeListener() {
	            @Override
	            public void textChange(final TextChangeEvent event) {
	                ic.removeAllContainerFilters();
	                ic.addContainerFilter(new Filter() {
	                    @Override
	                    public boolean passesFilter(Object itemId, Item item)
	                            throws UnsupportedOperationException {

	                        if (event.getText() == null
	                                || event.getText().equals("")) {
	                            return true;
	                        }
	                      
	                        return filterByProperty("Branch", item,
	                                event.getText())
	                                || filterByProperty("Store", item,
	                                        event.getText())
	                                || filterByProperty("Phone", item,
	                                        event.getText());

	                    }

	               
	                    public boolean appliesToProperty(Object propertyId) {
	                        if (propertyId.equals("Branch")
	                                || propertyId.equals("Store")
	                                || propertyId.equals("Phone"))
	                            return true;
	                        return false;
	                    }
	                });
	            }
	        });
	        
	        filter.setInputPrompt("Filter");
	        filter.addShortcutListener(new ShortcutListener("Clear",
	                KeyCode.ESCAPE, null) {
	            @Override
	            public void handleAction(Object sender, Object target) {
	                filter.setValue("");
	                ic.removeAllContainerFilters();
	            }
	        });
		 
		 HorizontalLayout ExporterHoldBtns = new HorizontalLayout();
		 ExporterHoldBtns.setSpacing(true);
		 
		         pdfExporter = new PdfExporter(terminaltable);
		        pdfExporter.setCaption("Export to PDF");
		        pdfExporter.addStyleName("default");
		       // pdfExporter.setIcon(getResource("../runo/icons/16/ok.png"));
		        pdfExporter.setDownloadFileName("TerminalsPdf"+dateFormat.format(new Date()));
		        pdfExporter.setWithBorder(false);
		        
		        excelExporter = new ExcelExporter(terminaltable);
		        excelExporter.setCaption("Export to Excel");
		        excelExporter.addStyleName("default");
		        excelExporter.setDownloadFileName("TerminalsExcel"+dateFormat.format(new Date()));
		        
		        ExporterHoldBtns.addComponent(filter);
		        ExporterHoldBtns.setExpandRatio(filter, 1);
		        ExporterHoldBtns.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
		        
		        ExporterHoldBtns.addComponent(excelExporter);			       
		        ExporterHoldBtns.addComponent(pdfExporter);
		        Exporterlayout.addComponent(ExporterHoldBtns);
		        Exporterlayout.setComponentAlignment(ExporterHoldBtns, Alignment.TOP_RIGHT);
		        
		        
		        return Exporterlayout;
	 }
	  private boolean filterByProperty(String prop, Item item, String text) {
	        if (item == null || item.getItemProperty(prop) == null
	                || item.getItemProperty(prop).getValue() == null)
	            return false;
	        String val = item.getItemProperty(prop).getValue().toString().trim()
	                .toLowerCase();
	        if (val.startsWith(text.toLowerCase().trim()))
	            return true;
	        // String[] parts = text.split(" ");
	        // for (String part : parts) {
	        // if (val.contains(part.toLowerCase()))
	        // return true;
	        //
	        // }
	        return false;
	    }
	 
	 public Window addTerminal()
	 {
		 Window AddTerminalwindow = new Window("Add Terminal");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		 AddTerminalwindow.center();
		 
		    FormLayout   AddTerminalform = new FormLayout();
		    AddTerminalform.addStyleName("outlined");
		    AddTerminalform.setMargin(true);
		    AddTerminalform.setSpacing(true);
			
	        final TextField  ImeiNo=new TextField("IMEI No", "");
	        ImeiNo.setRequired(true);
	        ImeiNo.setRequiredError("please enter EMEI");
	      
	       
	       	        
	        final TextField  TSerial=new TextField("Serial No", "");	        
	        TSerial.setRequired(true);
	        TSerial.setRequiredError("please enter serial");
	        
	        

	        final TextField  AppId=new TextField("App Id");
	        //AppId.setCaption("App Id");
	        //Sphone.setRequired(true);
	       // Sphone.setRequiredError("please enter app");App Id
	        
	        final TextField  AppVersion=new TextField("App Version", "");
	        
	        
	        final TextField  Tphone=new TextField("Phone", "");
	        
	       final ComboBox Ttype = new ComboBox ("Type");
	        
	        Ttype.addItem("POS");
	        Ttype.addItem("Phone");
	        Ttype.addItem("PC");
	        Ttype.setRequired(true);
	        Ttype.setRequiredError("please select device type");
	        Ttype.setNullSelectionAllowed(false);
	        Ttype.setWidth("160px");
	        
	        final TextField  Minamount=new TextField("Min Amount");
	        Minamount.setCaption("Min Amount");
	       // Minamount.setRequired(true);
	        Minamount.setRequiredError("please enter amount");
	        
	        final TextField  Maxamount=new TextField("Max Amount");
	        Maxamount.setCaption("Max Amount");
	       // Maxamount.setRequired(true);
	        Maxamount.setRequiredError("please enter amount");
	        
		   final ComboBox TStatus = new ComboBox ("Status");
		        
		   TStatus.addItem("Idle");
		   TStatus.addItem("Running");
		   TStatus.addItem("suspended");
		   TStatus.addItem("broken");
		   TStatus.setRequired(true);
		   TStatus.setRequiredError("please select device status");
		   TStatus.setNullSelectionAllowed(false);
		   TStatus.setWidth("160px");
	    
		
		   
		   final ComboBox TStorebranch = new ComboBox ("Branch");
		   TStorebranch.setNullSelectionAllowed(false);
		   //TStorebranch.setRequired(true);
		   //TStorebranch.setRequiredError("please select Store branch");
		   TStorebranch.setWidth("160px");
		  // TStorebranch.setEnabled(false);
	        
		   final ComboBox TStorePopBranch=new TerminalModel().DoFetchBranchlist("SELECT * FROM Store_branches ",TStorebranch);
	
		  
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					ImeiNo.validate();
					TSerial.validate();
					Ttype.validate();
					TStatus.validate();
					//TStorePopBranch.validate();
			
				
				
					//addDbStore(name.getValue(),Semail.getValue(),Sphone.getValue(),Saddress.getValue());
					
					addDbTerminal(ImeiNo.getValue(),TSerial.getValue(), AppId.getValue(),
							AppVersion.getValue(),Tphone.getValue(),Ttype.getValue().toString(),getTStatus(TStatus.getValue().toString()),
							getStoreID(TStorePopBranch.getValue().toString()),Minamount.getValue(),Maxamount.getValue());
				
					
					ImeiNo.setValue("");
					TSerial.setValue("");
					AppId.setValue("");
					AppVersion.setValue("");
					Tphone.setValue("");
					Minamount.setValue("");
					Maxamount.setValue("");
					terminaltable.setContainerDataSource(terminalData("SELECT * FROM Terminals "));
					
					Notification.show("Terminal Added");
				
			
						
				
					
				}
			});
	        	        
			AddTerminalform.addComponent(ImeiNo);
			AddTerminalform.addComponent(TSerial);
			AddTerminalform.addComponent(AppId);
			AddTerminalform.addComponent(AppVersion);
			AddTerminalform.addComponent(Tphone);
			AddTerminalform.addComponent(Ttype);
			AddTerminalform.addComponent(Minamount);
			AddTerminalform.addComponent(Maxamount);
			AddTerminalform.addComponent(TStatus);
			
			AddTerminalform.addComponent(TStorePopBranch);
			AddTerminalform.addComponent(Submit);
	        
			AddTerminalwindow.setContent(AddTerminalform);
		 
		 return AddTerminalwindow;
	 }
	 
	 
	 public Window EditTerminal(String EImei,String Eserial,String EappId,
			 String EappVersion,String Ephone,final int terminalID,final Table terminaltable2,final String Eminamount,final String Emaxamount)
	 {
		 final Window AddTerminalwindow = new Window("Edit Terminal");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		 AddTerminalwindow.center();
		 
		    FormLayout   AddTerminalform = new FormLayout();
		    AddTerminalform.addStyleName("outlined");
		    AddTerminalform.setMargin(true);
		    AddTerminalform.setSpacing(true);
			
	        final TextField  ImeiNo=new TextField("IMEI No", "");
	        ImeiNo.setRequired(true);
	        ImeiNo.setRequiredError("please enter EMEI");
	        ImeiNo.setValue(EImei);
	      
	       
	       	        
	        final TextField  TSerial=new TextField("Serial No", "");	        
	        TSerial.setRequired(true);
	        TSerial.setRequiredError("please enter serial");
	        TSerial.setValue(Eserial);
	        
	        

	        final TextField  AppId=new TextField("App Id", "");
	        AppId.setValue(EappId);
	        //Sphone.setRequired(true);
	       // Sphone.setRequiredError("please enter app");
	        
	        final TextField  AppVersion=new TextField("App Version", "");
	        AppVersion.setValue(EappVersion);
	        
	        final TextField  Tphone=new TextField("Phone", "");
	        
	        Tphone.setValue(Ephone);
	        
	       final ComboBox Ttype = new ComboBox ("Type");
	        
	        Ttype.addItem("POS");
	        Ttype.addItem("Phone");
	        Ttype.addItem("PC");
	        Ttype.setRequired(true);
	        Ttype.setRequiredError("please select device type");
	        Ttype.setNullSelectionAllowed(false);
	        Ttype.setWidth("160px");
	        
	        final TextField  Minamount=new TextField("Min Amount");
	        Minamount.setCaption("Min Amount");
	        Minamount.setValue(Eminamount);
	        Minamount.setRequiredError("please enter amount");
	        
	        final TextField  Maxamount=new TextField("Max Amount");
	        Maxamount.setCaption("Max Amount");
	        Maxamount.setValue(Emaxamount);
	        Maxamount.setRequiredError("please enter amount");
	        
		   final ComboBox TStatus = new ComboBox ("Status");
		        
		   TStatus.addItem("Idle");
		   TStatus.addItem("Running");
		   TStatus.addItem("suspended");
		   TStatus.addItem("broken");
		   TStatus.setRequired(true);
		   TStatus.setRequiredError("please select device status");
		   TStatus.setNullSelectionAllowed(false);
		   TStatus.setWidth("160px");
	    
		   
		   final ComboBox TStorebranch = new ComboBox ("Branch");
		   TStorebranch.setNullSelectionAllowed(false);
		   //TStorebranch.setRequired(true);
		   //TStorebranch.setRequiredError("please select Store branch");
		   TStorebranch.setWidth("160px");
		  // TStorebranch.setEnabled(false);
	        
		   final ComboBox TStorePopBranch=new TerminalModel().DoFetchBranchlist("SELECT * FROM Store_branches ",TStorebranch);
	
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					ImeiNo.validate();
					TSerial.validate();
					Ttype.validate();
					TStatus.validate();
					TStorePopBranch.validate();
			
					UpdateDbTerminal(ImeiNo.getValue(),TSerial.getValue(), AppId.getValue(),
						AppVersion.getValue(),Tphone.getValue(),Ttype.getValue().toString(),getTStatus(TStatus.getValue().toString()),
							getStoreID(TStorePopBranch.getValue().toString()),terminalID,Minamount.getValue(),Maxamount.getValue());
					
					
					
					
					ImeiNo.setValue("");
					TSerial.setValue("");
					AppId.setValue("");
					AppVersion.setValue("");
					Tphone.setValue("");
					Minamount.setValue("");
					Maxamount.setValue("");
					terminaltable=terminaltable2;
					terminaltable.setContainerDataSource(terminalData("SELECT * FROM Terminals "));
					UI.getCurrent().removeWindow (AddTerminalwindow);
					
					Notification.show("Terminal Updated");
				
			
						
				
					
				}
			});
	        	        
			AddTerminalform.addComponent(ImeiNo);
			AddTerminalform.addComponent(TSerial);
			AddTerminalform.addComponent(AppId);
			AddTerminalform.addComponent(AppVersion);
			AddTerminalform.addComponent(Tphone);
			AddTerminalform.addComponent(Ttype);
			AddTerminalform.addComponent(Minamount);
			AddTerminalform.addComponent(Maxamount);
			AddTerminalform.addComponent(TStatus);
			AddTerminalform.addComponent(TStorePopBranch);
			AddTerminalform.addComponent(Submit);
	        
			AddTerminalwindow.setContent(AddTerminalform);
		 
		 return AddTerminalwindow;
	 }
	 
	 
	 
	 public Table TerminalTable()
	 {
		 Table  terminaltable = new Table("");
		 
		terminaltable.setWidth("100%");
		terminaltable.setSizeFull();
		terminaltable.addStyleName("borderless");
		terminaltable.setSelectable(true);
		terminaltable.setColumnCollapsingAllowed(true);
		terminaltable.setColumnReorderingAllowed(true);
		terminaltable.setFooterVisible(true);
		 
		
		 
		
		terminaltable.setContainerDataSource(terminalData("SELECT * FROM Terminals "));
		
		 
		 return terminaltable;
	 }
	 
	 public Container terminalData(String Sqlquerry)
	 {
		 IndexedContainer icLoaded = new IndexedContainer();
		 icLoaded.removeAllItems();
		 
		 
		 
		 icLoaded.addContainerProperty("Imei No", String.class,  null);
		 icLoaded.addContainerProperty("Serial No",  String.class,  null);
		 icLoaded.addContainerProperty("App Id",       String.class, null);
		 icLoaded.addContainerProperty("App Version",       String.class, null);
		 icLoaded.addContainerProperty("Phone",       String.class, null);
		 icLoaded.addContainerProperty("Type",       String.class, null);
		 icLoaded.addContainerProperty("Min Amount",       Double.class, null);
		 icLoaded.addContainerProperty("Max Amount",       Double.class, null);
		 icLoaded.addContainerProperty("Store",       String.class, null);
		 icLoaded.addContainerProperty("Branch",       String.class, null);
		 icLoaded.addContainerProperty("Date",       String.class, null);
		 icLoaded.addContainerProperty("Status",       String.class, null);
		 icLoaded.addContainerProperty("Action",       HorizontalLayout.class, null);
		 
		 
		 ic=	new TerminalModel().DoFetchTerminaltable(Sqlquerry,icLoaded,terminaltable);
		 
		 return ic;
		 
	 }
	 
	 public void updateTable(Table terminaltable2){
		 terminaltable= terminaltable2;
		 terminaltable.setContainerDataSource(terminalData("SELECT * FROM Terminals  "));
	 }
	 public int getStoreID(String storeNmae)
	 {
		int storeId=0;
		
		if(!storeNmae.isEmpty())
		{
			storeId=new logins().DoFetch("SELECT * FROM Store_branches WHERE name='"+storeNmae.replace("(", ">>").split(">>")[0]+"'");
		}
		
		return storeId;
	 }
	 
	 
	 public void addDbTerminal(String imei,String serial, String appId,
			 String appversion,String phone, String type,int status,int storeID,String minamount,String maxamount)
	 {
		 int INImei=Integer.parseInt(imei);
		 String INAppID=appId;
		 String INAppVers=appversion;
		 String INphone=phone;
		 
		 if(appId.isEmpty())
		 {
			 INAppID="0";
		 }
		 
		 if(appversion.isEmpty())
		 {
			 INAppVers="0";
		 }
		 
		 
		 if(phone.isEmpty())
		 {
			 INphone="0";
		 }
		 
		
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'Terminals', 'created Terminals')");
		 
	
		 
		 int StoreInsert=new logins().DoInserts("INSERT INTO Terminals(IMEI_no,Serial_no,app_id,app_version,phone,type,status,Store_branches_fk,min_amount,max_amount,Log_fk) " +
		 		                "VALUES ("+INImei+", '"+serial+"', '"+INAppID+"','"+INAppVers+"','"+INphone+"','"+type+"'," +
		 		                		""+status+","+storeID+","+minamount+","+maxamount+","+theresult+")");
		
	 }
	 
	 public void UpdateDbTerminal(String imei,String serial, String appId,
			 String appversion,String phone, String type,int status,int storeBID,int UID,String minamount,String maxamount)
	 {
		 
		 int INImei=Integer.parseInt(imei);
		 String INAppID=appId;
		 String INAppVers=appversion;
		 String INphone=phone;
		 
		 if(appId.isEmpty())
		 {
			 INAppID="0";
		 }
		 
		 if(appversion.isEmpty())
		 {
			 INAppVers="0";
		 }
		 
		 
		 if(phone.isEmpty())
		 {
			 INphone="0";
		 }
		 
		 
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'Terminals', 'updated Terminals')");
		 
	
		 
		 new logins().DoUpdates("UPDATE  Terminals SET IMEI_no="+INImei+",Serial_no='"+serial+"',min_amount="+minamount+",max_amount="+maxamount+"," +
		 		"app_id='"+INAppID+"',app_version="+INAppVers+"," +
		 		                "phone='"+INphone+"',type='"+type+"',Store_branches_fk="+storeBID+",Log_fk="+theresult+" " +
		 		                                "WHERE UID="+UID+"");
		
	 }
	 
		public int getTStatus(String Status)
		{
			int Tstatus=0;
			
			if(Status.equalsIgnoreCase("idle"))
			{
				Tstatus=0;
			}
			
			else if(Status.equalsIgnoreCase("suspended"))
			{
				Tstatus=1;
			}
			
			else if(Status.equalsIgnoreCase("broken"))
			{
				Tstatus=2;
			}
			
			else if(Status.equalsIgnoreCase("running"))
			{
				Tstatus=3;
			}
			
			
			
			
			return Tstatus;
			
		}
	 
	
	 
	 public void enter(ViewChangeEvent event)
	 {
       String UserIsloged=UI.getCurrent().getSession().getAttribute("nameUser").toString();
		 
		 if(UserIsloged.length()<1)
		 {
			 UI.getCurrent().getSession().close();
			 navigator1.navigateTo(""); 
		 }
	    }
}
