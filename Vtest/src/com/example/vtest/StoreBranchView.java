package com.example.vtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.vaadin.haijian.ExcelExporter;
import org.vaadin.haijian.PdfExporter;

import com.example.model.StoreBranchModel;
import com.example.model.TerminalModel;
import com.example.model.dbsettings;
import com.example.model.logins;
import com.vaadin.data.Item;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutListener;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;

public class StoreBranchView  extends VerticalLayout implements View
{
	Navigator navigator1;
	PdfExporter pdfExporter;
    ExcelExporter  excelExporter;
    IndexedContainer ic;
    Table storetable1;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	 public VerticalLayout StoreBranchView1 ()
	 {
		       //avigator1=navigator;
		       // setSizeFull();
		       // addComponent(new header().myheader(navigator1));
	        
	       // Panel Branchpanel = new Panel("Stores Branches");
	       // Branchpanel.setSizeFull();
	        
	        final VerticalLayout Branchlayout = new VerticalLayout();
	      
	        Branchlayout.setCaption("Stores Branches");
	        Branchlayout.setMargin(true);	        
	        Branchlayout.setSpacing(true);
	        Branchlayout.addStyleName("toolbar");
	        
	        Label title = new Label("Stores Branches");
	        title.addStyleName("h1");
	        title.setSizeUndefined();
	        Branchlayout.addComponent(title);
	        Branchlayout.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
	        
			Button Branchbutton = new Button("Add Branch");
			Branchbutton.addStyleName("default");

			Branchbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					UI.getCurrent().addWindow(addStoreBranch());
					
						//	Notification.show("we want to add a user");
						
				
					
				}
			});
			
			Branchlayout.addComponent(Branchbutton);
			storetable1=Sbranchtable();
			Branchlayout.addComponent(dataExporter());
			Branchlayout.addComponent(storetable1);
		
			//Branchpanel.setContent(Branchlayout);
			//addComponent(Branchpanel);
	        return Branchlayout;
	 }
	public StoreBranchView(){
		 
	 }
	 public HorizontalLayout dataExporter()
	 {
		 HorizontalLayout Exporterlayout = new HorizontalLayout();
		// Exporterlayout.setMargin(true);
		// Exporterlayout.setSpacing(true);
		 
		 
		 Exporterlayout.setWidth("100%");
		 final TextField filter = new TextField();
	        filter.addTextChangeListener(new TextChangeListener() {
	            @Override
	            public void textChange(final TextChangeEvent event) {
	                ic.removeAllContainerFilters();
	                ic.addContainerFilter(new Filter() {
	                    @Override
	                    public boolean passesFilter(Object itemId, Item item)
	                            throws UnsupportedOperationException {

	                        if (event.getText() == null
	                                || event.getText().equals("")) {
	                            return true;
	                        }
	                      
	                        return filterByProperty("Name", item,
	                                event.getText())
	                                || filterByProperty("Email", item,
	                                        event.getText())
	                                || filterByProperty("Phone", item,
	                                        event.getText());

	                    }

	               
	                    public boolean appliesToProperty(Object propertyId) {
	                        if (propertyId.equals("Name")
	                                || propertyId.equals("Email")
	                                || propertyId.equals("Phone"))
	                            return true;
	                        return false;
	                    }
	                });
	            }
	        });
	        
	        filter.setInputPrompt("Filter");
	        filter.addShortcutListener(new ShortcutListener("Clear",
	                KeyCode.ESCAPE, null) {
	            @Override
	            public void handleAction(Object sender, Object target) {
	                filter.setValue("");
	                ic.removeAllContainerFilters();
	            }
	        });
		 
		 HorizontalLayout ExporterHoldBtns = new HorizontalLayout();
		 ExporterHoldBtns.setSpacing(true);
		 
		 
		 
		 
		        pdfExporter = new PdfExporter(storetable1);
		        pdfExporter.setCaption("Export to PDF");
		        //pdfExporter.addStyleName("sweetexpot");
		        //pdfExporter.setStyleName("sweetexpot");
		        pdfExporter.addStyleName("default");
		        pdfExporter.setDownloadFileName("StorebranchPdf"+dateFormat.format(new Date()));
		        pdfExporter.setWithBorder(false);
		        
		        excelExporter = new ExcelExporter(storetable1);
		        excelExporter.setCaption("Export to Excel");
		       // excelExporter.addStyleName("sweetexpot");
		        //excelExporter.setStyleName("sweetexpot");
		        excelExporter.addStyleName("default");
		        excelExporter.setDownloadFileName("StorebranchExcel"+dateFormat.format(new Date()));
		        
		        ExporterHoldBtns.addComponent(filter);
		        ExporterHoldBtns.setExpandRatio(filter, 1);
		        ExporterHoldBtns.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
		        
		        ExporterHoldBtns.addComponent(excelExporter);			       
		        ExporterHoldBtns.addComponent(pdfExporter);
		        
		       
		        
		        Exporterlayout.addComponent(ExporterHoldBtns);
		        Exporterlayout.setComponentAlignment(ExporterHoldBtns, Alignment.TOP_RIGHT);
		        
		        
		        return Exporterlayout;
	 }
	  private boolean filterByProperty(String prop, Item item, String text) {
	        if (item == null || item.getItemProperty(prop) == null
	                || item.getItemProperty(prop).getValue() == null)
	            return false;
	        String val = item.getItemProperty(prop).getValue().toString().trim()
	                .toLowerCase();
	        if (val.startsWith(text.toLowerCase().trim()))
	            return true;
	        // String[] parts = text.split(" ");
	        // for (String part : parts) {
	        // if (val.contains(part.toLowerCase()))
	        // return true;
	        //
	        // }
	        return false;
	    }
	  
	 public Window addStoreBranch()
	 {
		 Window Addbrachwindow = new Window("Add Store Branch");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		 Addbrachwindow.center();
		 
		    FormLayout   AddBranchform = new FormLayout();
		    AddBranchform.addStyleName("outlined");
		    AddBranchform.setMargin(true);
		    AddBranchform.setSpacing(true);
			
	        final TextField  Bname=new TextField("Name", "");
	        Bname.setRequired(true);
	        Bname.setRequiredError("please enter name");
	      
	       
	       	        
	        final TextField  Bemail=new TextField("Email", "");	        
	        Bemail.setRequired(true);
	        Bemail.setRequiredError("please enter email");
	        
	        

	        final TextField  Bphone=new TextField("Phone", "");
	        Bphone.setRequired(true);
	        Bphone.setRequiredError("please enter phone");
	        

		   final ComboBox TStore = new ComboBox ("Store");
		   TStore.setNullSelectionAllowed(true);
		   TStore.setRequired(true);
		   TStore.setWidth("160px");
		   
		 
		   final ComboBox TStorePop=new TerminalModel().DoFetchStorelist("SELECT * FROM Stores ",TStore);
	
		  
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					Bname.validate();
					Bemail.validate();
					Bphone.validate();
					TStorePop.validate();
			
					addDbStoreBranch(Bname.getValue(),Bemail.getValue(),Bphone.getValue(),getBStoreID(TStorePop.getValue().toString()));
					
					Bname.setValue("");
					Bemail.setValue("");
					Bphone.setValue("");
					TStorePop.setValue("");
				
					 storetable1.setContainerDataSource(storeData("SELECT * FROM Store_branches  "));
					
					Notification.show("Branch Added");
				
			
						
				
					
				}
			});
	        	        
			AddBranchform.addComponent(Bname);
			AddBranchform.addComponent(Bemail);
			AddBranchform.addComponent(Bphone);
			AddBranchform.addComponent(TStorePop);
			AddBranchform.addComponent(Submit);
	        
			Addbrachwindow.setContent(AddBranchform);
		 
		 return Addbrachwindow;
	 }
	 
	 
	 public Window EditStoreBranch(String Sname,String Semail,String Sphone,int storId,final int branchId,final Table storetable2)
	 {
		 final Window Addbrachwindow = new Window("Edit Store Branch");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		 Addbrachwindow.center();
		 
		    FormLayout   AddBranchform = new FormLayout();
		    AddBranchform.addStyleName("outlined");
		    AddBranchform.setMargin(true);
		    AddBranchform.setSpacing(true);
			
	        final TextField  Bname=new TextField("Name", "");
	        Bname.setRequired(true);
	        Bname.setRequiredError("please enter name");
	        Bname.setValue(Sname);
	      
	       
	       	        
	        final TextField  Bemail=new TextField("Email", "");	        
	        Bemail.setRequired(true);
	        Bemail.setRequiredError("please enter email");
	        Bemail.setValue(Semail);
	        
	        

	        final TextField  Bphone=new TextField("Phone", "");
	        Bphone.setRequired(true);
	        Bphone.setRequiredError("please enter phone");
	        Bphone.setValue(Sphone);
	        

		   final ComboBox TStore = new ComboBox ("Store");
		   TStore.setNullSelectionAllowed(true);
		   TStore.setRequired(true);
		   TStore.setValue(new StoreBranchModel().DoFetchBranchName(storId,"name"));
		   TStore.setWidth("160px");
		
		   
		  
	        
		   final ComboBox TStorePop=new TerminalModel().DoFetchStorelist("SELECT * FROM Stores ",TStore);
	
		  
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					Bname.validate();
					Bemail.validate();
					Bphone.validate();
					TStorePop.validate();
			
					UpdateDbBranch(Bname.getValue(),Bemail.getValue(),Bphone.getValue(),getBStoreID(TStorePop.getValue().toString()),branchId);
					
					Bname.setValue("");
					Bemail.setValue("");
					Bphone.setValue("");
					TStorePop.setValue("");
				
					 
					UI.getCurrent().removeWindow (Addbrachwindow);
					storetable1=storetable2;
					storetable1.setContainerDataSource(storeData("SELECT * FROM Store_branches  "));
					Notification.show("Branch Updated");
				
			
						
				
					
				}
			});
	        	        
			AddBranchform.addComponent(Bname);
			AddBranchform.addComponent(Bemail);
			AddBranchform.addComponent(Bphone);
			AddBranchform.addComponent(TStorePop);
			AddBranchform.addComponent(Submit);
	        
			Addbrachwindow.setContent(AddBranchform);
		 
		 return Addbrachwindow;
	 }
	 
	 
	 
	 public Table Sbranchtable()
	 {
		Table Sbranchtable = new Table("");
		 
		Sbranchtable.setSizeFull();
		Sbranchtable.addStyleName("borderless");
		Sbranchtable.setSelectable(true);
		Sbranchtable.setColumnCollapsingAllowed(true);
		Sbranchtable.setColumnReorderingAllowed(true);
		Sbranchtable.setFooterVisible(true);
		 
		 
		Sbranchtable.setContainerDataSource(storeData("SELECT * FROM Store_branches  "));
		 
		
	
			

		 //Table SbranchtableData=	new StoreBranchModel().DoFetchBranchtable("SELECT * FROM Store_branches ",Sbranchtable);
	        
		 
		 return Sbranchtable;
	 }
	 public IndexedContainer  storeData(String Sqlquerry)
	 {
		 IndexedContainer icLoaded = new IndexedContainer();
		 icLoaded.removeAllItems();
		 icLoaded.removeAllContainerFilters();
		 
		 icLoaded.addContainerProperty("Name", String.class,  null);
		 icLoaded.addContainerProperty("Email",  String.class,  null);
		 icLoaded.addContainerProperty("Phone",  String.class,  null);
		 icLoaded.addContainerProperty("Store",       String.class, null);
		 icLoaded.addContainerProperty("Date",       String.class, null);
		 icLoaded.addContainerProperty("Action",       HorizontalLayout.class, null);
		
		 ic=	new StoreBranchModel().DoFetchBranchtable(Sqlquerry,icLoaded,storetable1);
		 
		 return ic;
		 
	 }
	 
	 public void updateTable(Table storetable2){
		 storetable1= storetable2;
		 storetable1.setContainerDataSource(storeData("SELECT * FROM Store_branches  "));
	 }
	 public int getBStoreID(String storeNmae)
	 {
		int storeId=0;
		
		if(!storeNmae.isEmpty())
		{
			storeId=new logins().DoFetch("SELECT * FROM Stores WHERE name='"+storeNmae+"'");
		}
		
		return storeId;
	 }
	 public void addDbStoreBranch(String name,String email,String phone, int storeId)
	 {
	
		 
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'Store_branches', 'created Store branches')");
		 
	
		 
		 int StoreInsert=new logins().DoInserts("INSERT INTO Store_branches(name,email,phone,status,Store_fk,Log_fk) " +
		 		                " VALUES ('"+name+"', '"+email+"', '"+phone+"',0,"+storeId+","+theresult+")");
		
	 }
	 
	 public void UpdateDbBranch(String name,String email,String phone, int storeId,int UID)
	 {
		 
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'Store_branches', 'created Store branches')");
		 
	
		 
		 new logins().DoUpdates("UPDATE  Store_branches SET name='"+name+"',email='"+email+"',phone='"+phone+"',Store_fk="+storeId+",Log_fk="+theresult+" " +
		 		                                "WHERE UID="+UID+"");
		
	 }
	 
	 public void enter(ViewChangeEvent event)
	 {
 String UserIsloged=UI.getCurrent().getSession().getAttribute("nameUser").toString();
		 
		 if(UserIsloged.length()<1)
		 {
			 UI.getCurrent().getSession().close();
			 navigator1.navigateTo(""); 
		 }
	 }
}
