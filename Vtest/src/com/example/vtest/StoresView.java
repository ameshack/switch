package com.example.vtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.vaadin.haijian.ExcelExporter;
import org.vaadin.haijian.PdfExporter;

import com.example.model.TransactionLogModel;
import com.example.model.dbsettings;
import com.example.model.logins;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.data.Item;

public class StoresView  extends VerticalLayout implements View
{
	Navigator navigator1;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	  private TabSheet editors;
	  Table storetable1;
	   PdfExporter pdfExporter;
	     ExcelExporter  excelExporter;
	     IndexedContainer ic;
	 public StoresView ()
	 {
		   // navigator1=navigator;
		       // setSizeFull();
		      //  addComponent(new header().myheader(navigator1));
		   editors = new TabSheet();
	        editors.setSizeFull();
	        editors.addStyleName("borderless");
	        editors.addStyleName("editors");
	        addComponent(editors);
	        
	       
	        Panel Storepanel = new Panel();
	        Storepanel.setSizeFull();
	       
	        
	        final VerticalLayout Storelayout = new VerticalLayout();
	        Storelayout.setCaption("Stores");
	        Storelayout.setMargin(true);
	        Storelayout.setMargin(true);
	        Storelayout.setSpacing(true);
	        Storelayout.addStyleName("toolbar");
	        
	        Label title = new Label("Stores");
	        title.addStyleName("h1");
	        title.setSizeUndefined();
	        Storelayout.addComponent(title);
	        Storelayout.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
	        
			Button Storebutton = new Button("Add Store");
			//Storebutton.addStyleName("sweetadd");
			//Storebutton.setStyleName("sweetadd");
			Storebutton.addStyleName("default");

			Storebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					UI.getCurrent().addWindow(addStore());
					
						//	Notification.show("we want to add a user");
						
				
					
				}
			});
			
			Storelayout.addComponent(Storebutton);
			storetable1=StoreTable();
			Storelayout.addComponent(dataExporter());
			Storelayout.addComponent(storetable1);
		
			//Storepanel.setContent(Storelayout);
			//addComponent(Storepanel);
			 editors.addComponent(Storelayout);
			 editors.addComponent(new StoreBranchView().StoreBranchView1());
			 editors.addComponent(new TerminalView().TerminalView1());
	        
	 }
	 public HorizontalLayout dataExporter()
	 {
		 HorizontalLayout Exporterlayout = new HorizontalLayout();
		// Exporterlayout.setMargin(true);
		// Exporterlayout.setSpacing(true);
		 
		 
		 Exporterlayout.setWidth("100%");
		 final TextField filter = new TextField();
	        filter.addTextChangeListener(new TextChangeListener() {
	            @Override
	            public void textChange(final TextChangeEvent event) {
	                ic.removeAllContainerFilters();
	                ic.addContainerFilter(new Filter() {
	                    @Override
	                    public boolean passesFilter(Object itemId, Item item)
	                            throws UnsupportedOperationException {

	                        if (event.getText() == null
	                                || event.getText().equals("")) {
	                            return true;
	                        }
	                      
	                        return filterByProperty("Store Name", item,
	                                event.getText())
	                                || filterByProperty("Email", item,
	                                        event.getText())
	                                || filterByProperty("Phone", item,
	                                        event.getText());

	                    }

	               
	                    public boolean appliesToProperty(Object propertyId) {
	                        if (propertyId.equals("Store Name")
	                                || propertyId.equals("Email")
	                                || propertyId.equals("Phone"))
	                            return true;
	                        return false;
	                    }
	                });
	            }
	        });
	        
	        filter.setInputPrompt("Filter");
	        filter.addShortcutListener(new ShortcutListener("Clear",
	                KeyCode.ESCAPE, null) {
	            @Override
	            public void handleAction(Object sender, Object target) {
	                filter.setValue("");
	                ic.removeAllContainerFilters();
	            }
	        });
		 
		 HorizontalLayout ExporterHoldBtns = new HorizontalLayout();
		 ExporterHoldBtns.setSpacing(true);
		 
		 
		 
		 
		        pdfExporter = new PdfExporter(storetable1);
		        pdfExporter.setCaption("Export to PDF");
		        //pdfExporter.addStyleName("sweetexpot");
		        //pdfExporter.setStyleName("sweetexpot");
		        pdfExporter.addStyleName("default");
		        pdfExporter.setDownloadFileName("StoresPdf"+dateFormat.format(new Date()));
		        pdfExporter.setWithBorder(false);
		        
		        excelExporter = new ExcelExporter(storetable1);
		        excelExporter.setCaption("Export to Excel");
		       // excelExporter.addStyleName("sweetexpot");
		        //excelExporter.setStyleName("sweetexpot");
		        excelExporter.addStyleName("default");
		        excelExporter.setDownloadFileName("StoresExcel"+dateFormat.format(new Date()));
		        
		        ExporterHoldBtns.addComponent(filter);
		        ExporterHoldBtns.setExpandRatio(filter, 1);
		        ExporterHoldBtns.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
		        
		        ExporterHoldBtns.addComponent(excelExporter);			       
		        ExporterHoldBtns.addComponent(pdfExporter);
		        
		       
		        
		        Exporterlayout.addComponent(ExporterHoldBtns);
		        Exporterlayout.setComponentAlignment(ExporterHoldBtns, Alignment.TOP_RIGHT);
		        
		        
		        return Exporterlayout;
	 }
	  private boolean filterByProperty(String prop, Item item, String text) {
	        if (item == null || item.getItemProperty(prop) == null
	                || item.getItemProperty(prop).getValue() == null)
	            return false;
	        String val = item.getItemProperty(prop).getValue().toString().trim()
	                .toLowerCase();
	        if (val.startsWith(text.toLowerCase().trim()))
	            return true;
	        // String[] parts = text.split(" ");
	        // for (String part : parts) {
	        // if (val.contains(part.toLowerCase()))
	        // return true;
	        //
	        // }
	        return false;
	    }
	 
	 public Window addStore()
	 {
		 Window AddStorewindow = new Window("Add Store");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		 AddStorewindow.center();
		 
		    FormLayout   AddStoreform = new FormLayout();
		    AddStoreform.addStyleName("outlined");
		    AddStoreform.setMargin(true);
		    AddStoreform.setSpacing(true);
			
	        final TextField  name=new TextField("Store Name", "");
	        name.setRequired(true);
	        name.setRequiredError("please enter name");
	       
	       	        
	        final TextField  Semail=new TextField("Email", "");	        
	        Semail.setRequired(true);
	        Semail.setRequiredError("please enter email");
	        
	        

	        final TextField  Sphone=new TextField("Phone", "");
	        Sphone.setRequired(true);
	        Sphone.setRequiredError("please enter Phone");
	        
	        final TextArea  Saddress=new TextArea("Address", "");
	        Saddress.setRequired(true);
	        Saddress.setRequiredError("please enter Address");
	        

	       
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					name.validate();
					Semail.validate();
					Sphone.validate();
					Saddress.validate();
				
					addDbStore(name.getValue(),Semail.getValue(),Sphone.getValue(),Saddress.getValue());
					
					name.setValue("");
					Semail.setValue("");
					Sphone.setValue("");
					Saddress.setValue("");
					storetable1.setContainerDataSource(storeData("SELECT * FROM Stores  "));
				
					Notification.show("Store Added");
				
			
						
				
					
				}
			});
	        	        
			AddStoreform.addComponent(name);
			AddStoreform.addComponent(Semail);
			AddStoreform.addComponent(Sphone);
			AddStoreform.addComponent(Saddress);			
			AddStoreform.addComponent(Submit);
	        
			AddStorewindow.setContent(AddStoreform);
		 
		 return AddStorewindow;
	 }
	 
	 public Window EditStore(String Ename,String Eemail, String Ephone,String Eadress,final int EUID,final Table storetable2)
	 {
		 final Window AddStorewindow = new Window("Add Store");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		 AddStorewindow.center();
		 
		    FormLayout   AddStoreform = new FormLayout();
		    AddStoreform.addStyleName("outlined");
		    AddStoreform.setMargin(true);
		    AddStoreform.setSpacing(true);
			
	        final TextField  name=new TextField("Store Name", "");
	        name.setRequired(true);
	        name.setRequiredError("please enter name");
	        name.setValue(Ename);
	       
	       	        
	        final TextField  Semail=new TextField("Email", "");	        
	        Semail.setRequired(true);
	        Semail.setRequiredError("please enter email");
	        Semail.setValue(Eemail);
	        
	        

	        final TextField  Sphone=new TextField("Phone", "");
	        Sphone.setRequired(true);
	        Sphone.setRequiredError("please enter Phone");
	        Sphone.setValue(Ephone);
	        
	        final TextArea  Saddress=new TextArea("Address", "");
	        Saddress.setRequired(true);
	        Saddress.setRequiredError("please enter Address");
	        Saddress.setValue(Eadress);
	        

	       
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					name.validate();
					Semail.validate();
					Sphone.validate();
					Saddress.validate();
				
					UpdateDbStore(name.getValue(),Semail.getValue(),Sphone.getValue(),Saddress.getValue(),EUID);
					
					name.setValue("");
					Semail.setValue("");
					Sphone.setValue("");
					Saddress.setValue("");
					storetable1=storetable2;
					storetable1.setContainerDataSource(storeData("SELECT * FROM Stores  "));
					UI.getCurrent().removeWindow (AddStorewindow);
					
					Notification.show("Store Updated");
				
			
						
				
					
				}
			});
	        	        
			AddStoreform.addComponent(name);
			AddStoreform.addComponent(Semail);
			AddStoreform.addComponent(Sphone);
			AddStoreform.addComponent(Saddress);			
			AddStoreform.addComponent(Submit);
	        
			AddStorewindow.setContent(AddStoreform);
		 
		 return AddStorewindow;
	 }
	 
	 
	 public Table StoreTable()
	 {
		 Table storetable = new Table("");
		 
		 storetable.setSizeFull();
		 storetable.addStyleName("borderless");
		 storetable.setSelectable(true);
		 storetable.setColumnCollapsingAllowed(true);
		 storetable.setColumnReorderingAllowed(true);
		 storetable.setFooterVisible(true);
		 
		
		 
		
	
		storetable.setContainerDataSource(storeData("SELECT * FROM Stores  "));

		 //Table ustoretableData=	new logins().DoFetchStoretable("SELECT * FROM Stores ",storetable);
	        
		 
		 return storetable;
	 }
	 
	 public IndexedContainer  storeData(String Sqlquerry)
	 {
		 IndexedContainer icLoaded = new IndexedContainer();
		 icLoaded.removeAllItems();
		 icLoaded.removeAllContainerFilters();
		 
		 icLoaded.addContainerProperty("Store Name", String.class,  null);
		 icLoaded.addContainerProperty("Email",  String.class,  null);
		 icLoaded.addContainerProperty("Phone",  String.class,  null);
		 icLoaded.addContainerProperty("Address",       String.class, null);
		 icLoaded.addContainerProperty("Date",       String.class, null);
		 icLoaded.addContainerProperty("Action",       HorizontalLayout.class, null);
		
		 ic=	new logins().DoFetchStoretable(Sqlquerry,icLoaded,storetable1);
		 
		 return ic;
		 
	 }
	 public void updateTable(Table storetable2){
		 storetable1= storetable2;
		 storetable1.setContainerDataSource(storeData("SELECT * FROM Stores  "));
	 }
	 
	 
	 public void addDbStore(String name,String email, String phone,String adress)
	 {
		 
		
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'Stores', 'created store')");
		 
	
		 
		 int StoreInsert=new logins().DoInserts("INSERT INTO Stores(name,email,phone,address,status,Log_fk) " +
		 		                "VALUES ('"+name+"', '"+email+"', '"+phone+"','"+adress+"',0,"+theresult+")");
		
	 }
	 
	 public void UpdateDbStore(String name,String email, String phone,String adress,int UID)
	 {
		 
		
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'Stores', 'updated store')");
		 
	
		 
		 new logins().DoUpdates("UPDATE  Stores SET name='"+name+"',email='"+email+"',phone='"+phone+"',address='"+adress+"',Log_fk="+theresult+" " +
		 		                                "WHERE UID="+UID+"");
		
	 }
	 
	 public void enter(ViewChangeEvent event)
	 {
           String UserIsloged=UI.getCurrent().getSession().getAttribute("nameUser").toString();
		 
		 if(UserIsloged.length()<1)
		 {
			 UI.getCurrent().getSession().close();
			 navigator1.navigateTo(""); 
		 }
	    }
}
