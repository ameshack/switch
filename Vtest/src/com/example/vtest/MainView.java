package com.example.vtest;

import com.vaadin.data.Item;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;



public class MainView extends VerticalLayout implements View {
	Navigator navigator1;
	
    public MainView(Navigator navigator) {
        //setSizeFull();
    	
    	 //setMargin(true);
         setSpacing(true);
        this.navigator1=navigator;
        

        addComponent(new header().myheader(navigator1));
        
        Panel panelmain = new Panel("Reports");
		//panel.addStyleName("borderless bubble");
        panelmain.setSizeFull();
		
   
     
    }        

   
   
    public void enter(ViewChangeEvent event) {
 String UserIsloged=UI.getCurrent().getSession().getAttribute("nameUser").toString();
		 
		 if(UserIsloged.length()<1)
		 {
			 UI.getCurrent().getSession().close();
			 navigator1.navigateTo(""); 
		 }
    }
}