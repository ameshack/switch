package com.example.vtest;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * Main UI class
 */
@SuppressWarnings("serial")
@Theme("mycustom")
public class VtestUI extends UI {
	
	
	  Navigator navigator;
	   protected static final String MAINVIEW = "main";
	   protected static final String LOGINVIEW = "login";
	   protected static final String AdminUserVIEW = "adminuser";
	   protected static final String StoreVIEW = "storeview";
	   protected static final String terminalVIEW = "terminalview";
	   protected static final String storebranchVIEW = "storebranchlview";
	   protected static final String transactionLogVIEW = "transactionloglview";
	   protected static final String testVIEW = "testlview";

	@Override
	protected void init(VaadinRequest request) {
	
        navigator = new Navigator(this, this);
        
        // Create and register the views
      //  navigator.addView("", new Home(navigator));
        navigator.addView(MAINVIEW, new MainView(navigator));
        //navigator.addView(AdminUserVIEW, new AdminUsers(navigator));
       // navigator.addView(StoreVIEW, new StoresView(navigator));
        //navigator.addView(terminalVIEW, new TerminalView(navigator));
       // navigator.addView(storebranchVIEW, new StoreBranchView(navigator));
       // navigator.addView(transactionLogVIEW, new TransactionLogView(navigator));
        navigator.addView(testVIEW, new testView(navigator));
       
        
	}

}