package com.example.vtest;

import java.text.SimpleDateFormat;

import org.vaadin.haijian.ExcelExporter;
import org.vaadin.haijian.PdfExporter;

import com.example.model.StoreBranchModel;
import com.example.model.TerminalModel;
import com.example.model.TransactionLogModel;
import com.example.model.dbsettings;
import com.example.model.logins;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class TransactionLogView extends VerticalLayout implements View
{
	
	     Navigator navigator1;
	     PagedTable TransactionTable;
	     PdfExporter pdfExporter;
	     ExcelExporter  excelExporter;
	    
	   
	    TextField  PanNo;	
	   
	    DateField  fromdate;
	    HorizontalLayout UniversalHead;
	    DateField  Todate;
	    ComboBox SearchStore;
	    ComboBox SearchBranchchStore;
		
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	
	 public TransactionLogView ()
	 {
		// navigator1=navigator;
		 
		

		       // setSizeFull();
		        //addComponent(new header().myheader(navigator1));
		        Panel Transactionpanel = new Panel();
		       Transactionpanel.setSizeFull();
		        //Transactionpanel.setStyleName("light style");
		        
		    
		        
		        final VerticalLayout Branchlayout = new VerticalLayout();
		        Branchlayout.setMargin(true);
		        Branchlayout.setSpacing(true);
		        Branchlayout.addStyleName("toolbar");
		        
		        Label title = new Label("All Transactions");
		        title.addStyleName("h1");
		        title.setSizeUndefined();
		        Branchlayout.addComponent(title);
		        Branchlayout.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
		        
		        Branchlayout.addComponent(filterLay());
		        PagedTable myTable=TransactionTable();
		        Branchlayout.addComponent(dataExporter());
		        
				Branchlayout.addComponent(myTable);
				Branchlayout.addComponent(myTable.createControls());
			
				Transactionpanel.setContent(Branchlayout);
				addComponent(Transactionpanel); 
		 
		 
	

	 }
	 
	 public HorizontalLayout dataExporter()
	 {
		 HorizontalLayout Exporterlayout = new HorizontalLayout();
		// Exporterlayout.setMargin(true);
		// Exporterlayout.setSpacing(true);
		 Exporterlayout.setWidth("100%");
		 HorizontalLayout ExporterHoldBtns = new HorizontalLayout();
		 ExporterHoldBtns.setSpacing(true);
		 
		        pdfExporter = new PdfExporter(TransactionTable);
		        pdfExporter.setCaption("Export to PDF");
		        //pdfExporter.addStyleName("sweetexpot");
		        //pdfExporter.setStyleName("sweetexpot");
		        pdfExporter.addStyleName("default");
		        pdfExporter.setDownloadFileName("Transactions");
		        pdfExporter.setWithBorder(false);
		        
		        excelExporter = new ExcelExporter(TransactionTable);
		        excelExporter.setCaption("Export to Excel");
		       // excelExporter.addStyleName("sweetexpot");
		        //excelExporter.setStyleName("sweetexpot");
		        excelExporter.addStyleName("default");
		        excelExporter.setDownloadFileName("Transactions");
		        
		        
		        ExporterHoldBtns.addComponent(excelExporter);			       
		        ExporterHoldBtns.addComponent(pdfExporter);
		        Exporterlayout.addComponent(ExporterHoldBtns);
		        Exporterlayout.setComponentAlignment(ExporterHoldBtns, Alignment.TOP_RIGHT);
		        
		        
		        return Exporterlayout;
	 }
	 
	 public PagedTable TransactionTable()
	 {
		 TransactionTable = new PagedTable("");
		 
		//TransactionTable.setWidth("100%");
		 
		TransactionTable.setSizeFull();
		TransactionTable.addStyleName("borderless");
		TransactionTable.setSelectable(true);
		TransactionTable.setColumnCollapsingAllowed(true);
		TransactionTable.setColumnReorderingAllowed(true);
		TransactionTable.setFooterVisible(true);
		TransactionTable.setColumnFooter("Time", "Total");
		
		TransactionTable.setContainerDataSource(transData("SELECT * FROM Transaction_log "));
		 
		
	
			

		// Table TransactionTableData=	new TransactionLogModel().DoFetchBranchtable("SELECT * FROM Transaction_log ",TransactionTable);
	        
		 
		 return TransactionTable;
	 }
	 
	 public Container  transData(String Sqlquerry)
	 {
		 Container ic = new IndexedContainer();
		 ic.removeAllItems();
		 
		 
		 ic.addContainerProperty("Account No", String.class,  null);
		 ic.addContainerProperty("TCode",  String.class,  null);
		 ic.addContainerProperty("Store",  String.class,  null);
		 ic.addContainerProperty("Store branch",       String.class, null);
		 ic.addContainerProperty("Amount",       Double.class, null);
		 ic.addContainerProperty("Type",       String.class, null);
		 ic.addContainerProperty("Date",       String.class, null);
		 ic.addContainerProperty("Status",       String.class, null);
		 Container icLoaded=	new TransactionLogModel().DoFetchBranchtable(Sqlquerry,ic);
		 
		 return icLoaded;
		 
	 }
	 
	 public HorizontalLayout filterLay()
	 {
		 
		   final HorizontalLayout filterlayout = new HorizontalLayout();
		   filterlayout.setMargin(true);
		   filterlayout.setSpacing(true);
		   filterlayout.addStyleName("toolbar");
		  
		   
		   PanNo=new TextField("Account number", "");	
		   
		   
		   fromdate=new DateField("From");
		   fromdate.addStyleName("default");
		   fromdate.setDateFormat("yyyy-MM-dd");
		  
		   
		   Todate=new DateField("To");
		   Todate.addStyleName("default");
		   Todate.setDateFormat("yyyy-MM-dd");
		   
		    SearchStore= storeList();
		    SearchBranchchStore= storeBranchList();
		   
		   
		   filterlayout.addComponent(PanNo);
		   filterlayout.addComponent(SearchStore);
		   filterlayout.addComponent(SearchBranchchStore);
		   filterlayout.addComponent(fromdate);
		   filterlayout.addComponent(Todate);
		   
		   Button SearchThis =searchButton();
		   filterlayout.addComponent(SearchThis);
		   filterlayout.setComponentAlignment(SearchThis, Alignment.BOTTOM_RIGHT);
		   return filterlayout;
		 
	 }
	 
	 public ComboBox storeList()
	 {
		 ComboBox   TStore = new ComboBox ("Store");
		   TStore.setNullSelectionAllowed(true);
		 
		   TStore.setWidth("160px");
		   

	        
		   final ComboBox TStorePop=new TerminalModel().DoFetchStorelist("SELECT * FROM Stores ",TStore); 
		   
		   return TStorePop;
	 }
	 
	
	 public ComboBox storeBranchList()
	 {
		 ComboBox  TStorebranch = new ComboBox ("Branch");
		  // TStorebranch.setNullSelectionAllowed(false);
		   //TStorebranch.setRequired(true);
		   //TStorebranch.setRequiredError("please select Store branch");
		   TStorebranch.setWidth("160px");
		  // TStorebranch.setEnabled(false);
	        
		   final ComboBox TStorePopBranch=new TerminalModel().DoFetchBranchlist("SELECT * FROM Store_branches ",TStorebranch);
		   
		   return TStorePopBranch;
	
	 }
	 
	 public Button searchButton()
	 {
			Button Search = new Button("Search");
			Search.addStyleName("default");
			
			Search.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					
					// Notification.show(getQuery());

					if(fromdate.getValue()!=null)
					{
						if(Todate.getValue()==null)
						{
							Notification.show("Please enter To date ");
						}
						else
						{
							
						
							
							TransactionTable.setContainerDataSource(transData(getQuery()));
							pdfExporter.setTableToBeExported(TransactionTable);
							excelExporter.setTableToBeExported(TransactionTable);
							//Notification.show(getQuery());
						}
						
						
					}
					

					else if(Todate.getValue()!=null)
					{
						if(fromdate.getValue()==null)
						{
							Notification.show("Please enter From date ");
						}
						
						else
						{
							TransactionTable.setContainerDataSource(transData(getQuery()));
							pdfExporter.setTableToBeExported(TransactionTable);
							excelExporter.setTableToBeExported(TransactionTable);
							//Notification.show(getQuery());
						}
					}
					
					else if(Todate.getValue()==null && fromdate.getValue()==null)
					{
						TransactionTable.setContainerDataSource(transData(getQuery()));
						 pdfExporter.setTableToBeExported(TransactionTable);
						 excelExporter.setTableToBeExported(TransactionTable);
						//Notification.show(getQuery());
						
					}
				
						
						
				
					 
				}
			});
			
			return Search;
	 }
	 
	 public String  getQuery()
	 {
		 String myPan="";
		 String myfromDate="";
		 String storeName="";
		 String branchName="";
		 
	
		 
		if(PanNo.getValue().length()>0)
		{
			myPan= " AND PAN="+PanNo.getValue();
			
		}
		
		if(SearchStore.getValue()!=null)
		{
			storeName= " AND Store_branches_fk IN ("+new TerminalModel().DoFetchStoreBranchIds(SearchStore.getValue().toString())+")";
			
		}
		
		if(SearchBranchchStore.getValue()!=null)
		{
			storeName="";
			
			branchName=" AND Store_branches_fk="+new logins().DoFetch("SELECT * FROM Store_branches WHERE name='"+SearchBranchchStore.getValue().toString().replace("(", ">>").split(">>")[0].trim()+"'");
		}
		
		
		if(fromdate.getValue()!=null)
		{
			if(Todate.getValue()!=null)
			{
				myfromDate= " AND Transmission_date BETWEEN '"+sdf.format(fromdate.getValue())+"' AND '"+sdf.format(Todate.getValue())+"'";
			}
			
		}
		
		
	
		
		return "SELECT * FROM Transaction_log WHERE Terminal_fk>0 "+myPan+" "+storeName+" "+myfromDate+" "+branchName;
	 }
	 
	 public void enter(ViewChangeEvent event)
	 {
		 String UserIsloged=UI.getCurrent().getSession().getAttribute("nameUser").toString();
		 
		 if(UserIsloged.length()<1)
		 {
			 UI.getCurrent().getSession().close();
			 navigator1.navigateTo(""); 
		 }
		 
		 
			// UniversalHead=new header().myheader(navigator1);
		 
	      // Notification.show(UI.getCurrent().getSession().getAttribute("nameUser").toString());
	       
	 }
}
