package com.example.vtest;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Random;

import org.tepi.filtertable.FilterTable;

import com.example.vtest.FilterTableDemoUI.State;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomTable.RowHeaderMode;
import com.vaadin.ui.themes.Reindeer;

public class testView  extends VerticalLayout implements View
{
	
	 public testView (Navigator navigator)
	 {
	        setSizeFull();
	    
	 
	        
	    	FilterTable normalFilterTable = buildFilterTable();
			final VerticalLayout mainLayout = new VerticalLayout();
			mainLayout.setSizeFull();
			mainLayout.setSpacing(true);
			mainLayout.setMargin(true);
			mainLayout.addComponent(normalFilterTable);
			mainLayout.setExpandRatio(normalFilterTable, 1);
			mainLayout.addComponent(buildButtons(normalFilterTable));

			Panel p = new Panel("Test");
			p.setStyleName(Reindeer.PANEL_LIGHT);
			p.setSizeFull();
			p.setContent(mainLayout);

			addComponent(p);
	 }
	 
		private Component buildButtons(final FilterTable relatedFilterTable) {
			HorizontalLayout buttonLayout = new HorizontalLayout();
			buttonLayout.setHeight(null);
			buttonLayout.setWidth("100%");
			buttonLayout.setSpacing(true);

			Label hideFilters = new Label("Show Filters:");
			hideFilters.setSizeUndefined();
			buttonLayout.addComponent(hideFilters);
			buttonLayout.setComponentAlignment(hideFilters, Alignment.MIDDLE_LEFT);

			for (Object propId : relatedFilterTable.getContainerPropertyIds()) {
				Component t = createToggle(relatedFilterTable, propId);
				buttonLayout.addComponent(t);
				buttonLayout.setComponentAlignment(t, Alignment.MIDDLE_LEFT);
			}

			CheckBox showFilters = new CheckBox("Toggle Filter Bar visibility");
			showFilters.setValue(relatedFilterTable.isFilterBarVisible());
			showFilters.setImmediate(true);
			showFilters.addValueChangeListener(new Property.ValueChangeListener() {

				public void valueChange(ValueChangeEvent event) {
					relatedFilterTable.setFilterBarVisible((Boolean) event
					        .getProperty().getValue());

				}
			});
			buttonLayout.addComponent(showFilters);
			buttonLayout.setComponentAlignment(showFilters, Alignment.MIDDLE_RIGHT);
			buttonLayout.setExpandRatio(showFilters, 1);

			Button setVal = new Button("Set the State filter to 'Processed'");
			setVal.addClickListener(new Button.ClickListener() {

				public void buttonClick(ClickEvent event) {
					relatedFilterTable
					        .setFilterFieldValue("state", State.PROCESSED);
				}
			});
			buttonLayout.addComponent(setVal);

			Button reset = new Button("Reset");
			reset.addClickListener(new Button.ClickListener() {

				public void buttonClick(ClickEvent event) {
					relatedFilterTable.resetFilters();
				}
			});
			buttonLayout.addComponent(reset);

			return buttonLayout;
		}
		
		private Component createToggle(final FilterTable relatedFilterTable,
		        final Object propId) {
			CheckBox toggle = new CheckBox(propId.toString());
			toggle.setValue(relatedFilterTable.isFilterFieldVisible(propId));
			toggle.setImmediate(true);
			toggle.addValueChangeListener(new Property.ValueChangeListener() {

				public void valueChange(ValueChangeEvent event) {
					relatedFilterTable.setFilterFieldVisible(propId,
					        !relatedFilterTable.isFilterFieldVisible(propId));
				}
			});
			return toggle;
		}
		private FilterTable buildFilterTable() {
			FilterTable filterTable = new FilterTable();
			filterTable.setSizeFull();

			filterTable.setFilterDecorator(new DemoFilterDecorator());
			filterTable.setFilterGenerator(new DemoFilterGenerator());

			filterTable.setFilterBarVisible(true);

			filterTable.setSelectable(true);
			filterTable.setImmediate(true);
			filterTable.setMultiSelect(true);

			filterTable.setRowHeaderMode(RowHeaderMode.INDEX);

			filterTable.setColumnCollapsingAllowed(true);

			filterTable.setColumnReorderingAllowed(true);

			filterTable.setContainerDataSource(buildContainer());

			filterTable.setColumnCollapsed("state", true);

			filterTable.setVisibleColumns(new String[] { /* "name", */"id", "state",
			        "date", "validated", "checked" });

			return filterTable;
		}

		
	 
		@SuppressWarnings("unchecked")
		private Container buildContainer() {
			IndexedContainer cont = new IndexedContainer();
			Calendar c = Calendar.getInstance();

			cont.addContainerProperty("name", String.class, null);
			cont.addContainerProperty("id", Integer.class, null);
			cont.addContainerProperty("state", State.class, null);
			cont.addContainerProperty("date", Timestamp.class, null);
			cont.addContainerProperty("validated", Boolean.class, null);
			cont.addContainerProperty("checked", Boolean.class, null);

			Random random = new Random();
			for (int i = 0; i < 10000; i++) {
				cont.addItem(i);
				/* Set name and id properties */
				cont.getContainerProperty(i, "name").setValue("Order " + i);
				cont.getContainerProperty(i, "id").setValue(i);
				/* Set state property */
				int rndInt = random.nextInt(4);
				State stateToSet = State.CREATED;
				if (rndInt == 0) {
					stateToSet = State.PROCESSING;
				} else if (rndInt == 1) {
					stateToSet = State.PROCESSED;
				} else if (rndInt == 2) {
					stateToSet = State.FINISHED;
				}
				cont.getContainerProperty(i, "state").setValue(stateToSet);
				/* Set date property */
				cont.getContainerProperty(i, "date").setValue(
				        new Timestamp(c.getTimeInMillis()));
				c.add(Calendar.DAY_OF_MONTH, 1);
				/* Set validated property */
				cont.getContainerProperty(i, "validated").setValue(
				        random.nextBoolean());
				/* Set checked property */
				cont.getContainerProperty(i, "checked").setValue(
				        random.nextBoolean());
			}
			return cont;
		}

	 
	 public void enter(ViewChangeEvent event)
	 {
	      //  Notification.show("Admin users loaded");
	 }
}
