package com.example.vtest;

import org.vaadin.addon.borderlayout.BorderLayout;

import com.example.model.dbsettings;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.ClassResource;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

public class header  extends VerticalLayout{

	Navigator navigator1;
	
	
	public header(){
		
	}
	public HorizontalLayout myheader(Navigator navigator)
	{
		
	
		
	
		navigator1=navigator;

		HorizontalLayout headerBorder = new HorizontalLayout();
      
		headerBorder.setWidth("100%");
		headerBorder.setHeight("90px");
		headerBorder.addStyleName("mainScreenUpperPanel");
		headerBorder.setStyleName("mainScreenUpperPanel");
		
		HorizontalLayout UserDefiner = new HorizontalLayout();
		//UserDefiner.setSpacing(true);
		UserDefiner.setMargin(true);
		UserDefiner.setSpacing(true);
	
		
		//UserDefiner.addComponent(getMeUserHeader());
		UserDefiner.addComponent(LogoutBTN());
		
		
		HorizontalLayout Apptitle = new HorizontalLayout();
		Apptitle.setSpacing(true);
		Apptitle.setMargin(true);
       Label SwitchHeaderLb= new Label("Payment Gateway");
       SwitchHeaderLb.addStyleName("switchname");
       SwitchHeaderLb.setStyleName("switchname");
       Apptitle.addComponent(SwitchHeaderLb);
       
       
       headerBorder.addComponent(Apptitle);
       headerBorder.setComponentAlignment(Apptitle, Alignment.MIDDLE_LEFT);
       
       MenuBar userMenubar=HeaderMenubar();
       
       headerBorder.addComponent(userMenubar);
       headerBorder.setComponentAlignment(userMenubar, Alignment.MIDDLE_LEFT);
       headerBorder.addComponent(UserDefiner);
       headerBorder.setComponentAlignment(UserDefiner, Alignment.TOP_RIGHT);
		
		return headerBorder;
		
	
	
	
	
	}
	
	public Label getMeUserHeader()
	{	
		String theUserIS="teso"+UI.getCurrent().getSession().getAttribute("nameUser").toString();
		String mynametest="wewe";
		Label LogedUserNmaes= new Label();
		LogedUserNmaes.setValue(theUserIS);
		
		
		LogedUserNmaes.addStyleName("welcomename");
		LogedUserNmaes.setStyleName("welcomename");
		
		return LogedUserNmaes;
		
	}
	
	public Button LogoutBTN(){
		final  Button LogoutMe = new Button("Log Out");
		
		LogoutMe.setStyleName("link style");
			
		LogoutMe.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				
				UI.getCurrent().getSession().close();
				 navigator1.navigateTo(""); 
			}
		});
		return LogoutMe;
	}
	
	public MenuBar HeaderMenubar()
	{

        MenuBar menubar = new MenuBar();
        menubar.addStyleName("myheadmenu");
        menubar.setStyleName("myheadmenu");
       
       final MenuBar.MenuItem file = menubar.addItem("Users", null);
     
    //   final MenuBar.MenuItem newItem = file.addItem("New", null);
    //   file.addItem("Open file...", menuCommand);
       //file.addSeparator();

    //   newItem.addItem("File", menuCommand);
     //  newItem.addItem("Folder", menuCommand);
     //  newItem.addItem("Project...", menuCommand);

       file.addItem("Admin Users", menuCommand);
      // file.addItem("Close All", menuCommand);
       file.addSeparator();

      // file.addItem("Save", menuCommand);
       //file.addItem("Save As...", menuCommand);
      // file.addItem("Save All", menuCommand);

       final MenuBar.MenuItem edit = menubar.addItem("Stores", null);
       edit.addItem("Stores", menuCommand);
       edit.addItem("Store Branches", menuCommand);
       edit.addSeparator();

       edit.addItem("Store Users", menuCommand);
       edit.addItem("Store Settings", menuCommand);
      
       edit.addSeparator();


       final MenuBar.MenuItem view = menubar.addItem("Terminals", null);
       view.addItem("Terminals", menuCommand);
      // view.addItem("Customize Toolbar...", menuCommand);
       view.addSeparator();

       
       final MenuBar.MenuItem TransactionsLog = menubar.addItem("Transactions", menuCommand);
       
       return menubar;
	}
	
	   private Command menuCommand = new Command() {
	        public void menuSelected(MenuItem selectedItem) {
	           
	          
	            if(selectedItem.getText().equalsIgnoreCase("Admin Users"))
	            {
	            	navigator1.navigateTo("adminuser");
	            }
	            
	            
	            else if(selectedItem.getText().equalsIgnoreCase("Stores"))
	            {
	            	navigator1.navigateTo("storeview");
	            }
	            
	            else if(selectedItem.getText().equalsIgnoreCase("Terminals"))
	            {
	            	navigator1.navigateTo("terminalview");
	            }
	            
	            else if(selectedItem.getText().equalsIgnoreCase("Store Branches"))
	            {
	            	navigator1.navigateTo("storebranchlview");
	            }
	            
	            else if(selectedItem.getText().equalsIgnoreCase("Transactions"))
	            {
	            	navigator1.navigateTo("transactionloglview");
	            	
	            }
	            
	            else if(selectedItem.getText().equalsIgnoreCase("test table"))
	            {
	            	navigator1.navigateTo("testlview");
	            	
	            }
	            
	            else
	            {
	            	  Notification.show("Action " + selectedItem.getText());
	                  
	            }
	            
	        }
	    };    
}
