package com.example.vtest;

import com.example.model.ReportsModel;
import com.example.model.SendEmail;
import com.example.model.dbsettings;
import com.example.model.logins;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window;

public class AdminUsers extends VerticalLayout implements View
{
	
	Navigator navigator1;
	 private TabSheet editors;
	 Table usertable;
	 
	 public AdminUsers()
	 {
		  
		   editors = new TabSheet();
	        editors.setSizeFull();
	        editors.addStyleName("borderless");
	        editors.addStyleName("editors");
	        addComponent(editors);
	        
	        Panel Userpanel = new Panel();
	        Userpanel.setSizeFull();
	        
	        final VerticalLayout Userlayout = new VerticalLayout();
	        Userlayout.setCaption("Users");
	        Userlayout.setMargin(true);
	      
	        Userlayout.setSpacing(true);
	        Userlayout.addStyleName("toolbar");
	        
	        Label title = new Label("Users");
	        title.addStyleName("h1");
	        title.setSizeUndefined();
	        Userlayout.addComponent(title);
	        Userlayout.setComponentAlignment(title, Alignment.MIDDLE_LEFT);;
	        
	      
	        
	        
			Button Userbutton = new Button("Add User");
			Userbutton.addStyleName("default");

			Userbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					UI.getCurrent().addWindow(addAminUser());
					
						//	Notification.show("we want to add a user");
						
				
					
				}
			});
			
			//Userbutton.setWidth("100px");
			Userlayout.addComponent(Userbutton);
			Userlayout.addComponent(myuserTable());
		
			//Userpanel.setContent(Userlayout);
			//addComponent(Userpanel);
			 editors.addComponent(Userlayout);
			 editors.addComponent(new Emailsettings().Emailsettings());
		    	
			 
	        
		 
	 }

	 
	 public Window addAminUser()
	 {
		 Window AddadminUserwindow = new Window("Add User");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		    AddadminUserwindow.center();
		 
		    FormLayout   Adduserform = new FormLayout();
		    Adduserform.addStyleName("outlined");
	        Adduserform.setMargin(true);
	        Adduserform.setSpacing(true);
			
	        final TextField  fname=new TextField("First Name", "");
	        fname.setRequired(true);
	        fname.setRequiredError("please enter name");
	       
	       
	        final TextField  lname=new TextField("Last Name", "");
	        lname.setRequired(true);
	        lname.setRequiredError("please enter name");
	        
	        final TextField  Uemail=new TextField("Email", "");	        
	        Uemail.setRequired(true);
	        Uemail.setRequiredError("please enter email");
	        
	        final PasswordField  UPassword=new PasswordField("Password", "");	        
	        UPassword.setRequired(true);
	        UPassword.setRequiredError("please enter password");
	        
	        final PasswordField  CUPassword=new PasswordField("Confirm Password", "");
	        CUPassword.setRequired(true);
	        CUPassword.setRequiredError("please enter password");
	       
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					fname.validate();
					lname.validate();
					Uemail.validate();
					UPassword.validate();
					CUPassword.validate();
					
					if(UPassword.getValue().equals(CUPassword.getValue()))	
					{
					addDbUser(fname.getValue(),lname.getValue(),Uemail.getValue(),UPassword.getValue());
					
					String MyemailMsg="Hello "+fname.getValue() +" "+lname.getValue()+",\n \nAn account has been created for you with the following credentials:\n \n"
							+"Username: "+Uemail.getValue()+"\n"+"Password: "+UPassword.getValue()+"\n \nPlease change your Password as soon as you login. " +
									"\n \nThe Switch Team";
					
					new SendEmail().SendEmailOut("New Account Created", MyemailMsg, Uemail.getValue());
					
					fname.setValue("");
					lname.setValue("");
					Uemail.setValue("");
					UPassword.setValue("");
					CUPassword.setValue("");
					
					Notification.show("User Added");
					}
					
					else
					{
						Notification.show("Password mismatch!");
					}
					//Notification.show("Am awaiting Db operation");
						
				
					
				}
			});
	        	        
	        Adduserform.addComponent(fname);
	        Adduserform.addComponent(lname);
	        Adduserform.addComponent(Uemail);
	        Adduserform.addComponent(UPassword);
	        Adduserform.addComponent(CUPassword);
	        Adduserform.addComponent(Submit);
	        
	        AddadminUserwindow.setContent(Adduserform);
		 
		 return AddadminUserwindow;
	 }
	 
	 public Window EditAminUser(String fnameE,String lnameE,final String emailE,final int UID)
	 {
		final Window AddadminUserwindow = new Window("Edit User");
		 
		// AddadminUserwindow.setPositionX(500);
		 //AddadminUserwindow.setPositionY(500);
		    AddadminUserwindow.center();
		 
		    FormLayout   Adduserform = new FormLayout();
		    Adduserform.addStyleName("outlined");
	        Adduserform.setMargin(true);
	        Adduserform.setSpacing(true);
			
	        final TextField  fname=new TextField("First Name", "");
	        fname.setRequired(true);
	        fname.setRequiredError("please enter name");
	        fname.setValue(fnameE);
	       
	       
	        final TextField  lname=new TextField("Last Name", "");
	        lname.setRequired(true);
	        lname.setRequiredError("please enter name");
	        lname.setValue(lnameE);
	        
	        final TextField  Uemail=new TextField("Email", "");	        
	        Uemail.setRequired(true);
	        Uemail.setRequiredError("please enter email");
	        Uemail.setValue(emailE);
	        
	        final PasswordField  UPassword=new PasswordField("Password", "");	        
	        UPassword.setValue("12345");
	        UPassword.setEnabled(false);
	        
	        final PasswordField  CUPassword=new PasswordField("Confirm Password", "");
	        CUPassword.setValue("12345");
	        CUPassword.setEnabled(false);
	      
	       
	        
			Button Submit = new Button("Submit");
			Submit.addStyleName("default");
			Submit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {

					
					
					fname.validate();
					lname.validate();
					Uemail.validate();
					UPassword.validate();
					CUPassword.validate();
					
					if(UPassword.getValue().equals(CUPassword.getValue()))	
					{
						
						//shud be edit method
						addDbUserUpdate(fname.getValue(),lname.getValue(),Uemail.getValue(),UPassword.getValue(),UID);
						
						String MyemailMsg="Hello "+fname.getValue() +" "+lname.getValue()+",\n \nYour account has been changed if you did not make any changes please report to your Administrator" +
										"\n \nThe Switch Team";
						
						new SendEmail().SendEmailOut("New Account Created", MyemailMsg, emailE);
					
					fname.setValue("");
					lname.setValue("");
					Uemail.setValue("");
					UPassword.setValue("");
					CUPassword.setValue("");
					
					
					UI.getCurrent().removeWindow (AddadminUserwindow);
					
					Notification.show("User Updated");
					}
					
					else
					{
						Notification.show("Password mismatch!");
					}
					//Notification.show("Am awaiting Db operation");
						
				
					
				}
			});
	        	        
	        Adduserform.addComponent(fname);
	        Adduserform.addComponent(lname);
	        Adduserform.addComponent(Uemail);
	        Adduserform.addComponent(UPassword);
	        Adduserform.addComponent(CUPassword);
	        Adduserform.addComponent(Submit);
	        
	        AddadminUserwindow.setContent(Adduserform);
		 
		 return AddadminUserwindow;
	 }
	 
	 
	 public Table myuserTable()
	 {
		 usertable = new Table("");
		 
		usertable.setSizeFull();
		usertable.addStyleName("borderless");
		usertable.setSelectable(true);
		usertable.setColumnCollapsingAllowed(true);
		usertable.setColumnReorderingAllowed(true);
		usertable.setFooterVisible(true);
		 
		 
	
		 
		
		 usertable.setContainerDataSource(AdminUserData("SELECT * FROM User_entity"));
			
			

		// Table usertableData=	new logins().DoFetchRtable("SELECT * FROM User_entity ",usertable,"account");
	        
		 
		 return usertable;
	 }
	 
	
	 public IndexedContainer  AdminUserData(String Sqlquerry)
	 {
		 IndexedContainer icLoaded = new IndexedContainer();
		 icLoaded.removeAllItems();
		 icLoaded.removeAllContainerFilters();
		 
		 icLoaded.addContainerProperty("First Name", String.class,  null);
		 icLoaded.addContainerProperty("Last Name",  String.class,  null);
		 icLoaded.addContainerProperty("Email",  String.class,  null);
		 icLoaded.addContainerProperty("Date",  String.class,  null);
		 icLoaded.addContainerProperty("Action",  HorizontalLayout.class,  null);

		 IndexedContainer ic=new logins().DoFetchRtable(Sqlquerry, icLoaded, usertable,"admin");
		 
	
	
		 
		 return icLoaded;
		 
	 }
	 public void updateTable(Table terminaltable2){
		 usertable= terminaltable2;
		 usertable.setContainerDataSource(AdminUserData("SELECT * FROM User_entity  "));
	 }
	 
	 public void addDbUser(String fname,String lname, String email,String password)
	 {
		 
		
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES (1, 'User_entity', 'created user')");
		 
	
		 
		 int userInsert=new logins().DoInserts("INSERT INTO User_entity(fname,lname,email,status,Log_fk) " +
		 		                "VALUES ('"+fname+"', '"+lname+"', '"+email+"',0,"+theresult+")");
		 
		 int userLogin=new logins().DoInserts("INSERT INTO User_login(User_fk,usename,password,Log_fk) " +
	                "VALUES ("+userInsert+", '"+email+"', '"+password+"',"+theresult+")");
	 }
	 
	 public void addDbUserUpdate(String fname,String lname, String email,String password, int UID)
	 {
		 
		
		 int theresult =new logins().DoInserts("INSERT INTO Log_entity(User_fk,table_name,Sql_type) " +
	                "VALUES ("+dbsettings.LoggedUserIDState+", 'User_entity', 'updated user')");
		 
		
		 new logins().DoUpdates("UPDATE  User_entity SET fname='"+fname+"',lname='"+lname+"'," +
		 		                               "email='"+email+"',status=0,Log_fk="+theresult+"  " +
		 		                                " WHERE UID="+UID+"");
		 
		new logins().DoUpdates("UPDATE  User_login SET usename='"+email+"',Log_fk="+theresult+" " +
	                                          "WHERE User_fk="+UID+"");
	 }
	 
	 public void enter(ViewChangeEvent event)
	 {
        String UserIsloged=UI.getCurrent().getSession().getAttribute("nameUser").toString();
		 
		 if(UserIsloged.length()<1)
		 {
			 UI.getCurrent().getSession().close();
			 navigator1.navigateTo(""); 
		 }
	    }
	 
}
