package com.example.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



import com.example.vtest.AdminUsers;
import com.example.vtest.Emailsettings;
import com.example.vtest.MyaccountView;
import com.example.vtest.StoresView;
import com.example.vtest.StoreBranchView;
import com.example.vtest.TerminalView;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class logins 
{
	
	public logins(){
		
	}
	
	ResultSet returnedRS;
	   Navigator navigator;
	
	
	public void processQuerry(String sqlType,String SqlQuery)
	{
		
		if(sqlType.equalsIgnoreCase("insert"))
		{
			
		}
		else if(sqlType.equalsIgnoreCase("select"))
		{
			
		}
		
		else if(sqlType.equalsIgnoreCase("update"))
		{
			
		}
		
		else
		{
		 //throw sql error 	
		}
	}
	
	public int DoInserts(String passedSQL)
	{
		
		int theUID=0;
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       
        
        
       PreparedStatement ps=conn.prepareStatement(passedSQL,PreparedStatement.RETURN_GENERATED_KEYS);
       ps.executeUpdate();
       
       ResultSet rs = ps.getGeneratedKeys();
       if (rs.next()){
           theUID=rs.getInt(1);
       }
       rs.close();
       ps.close();
       conn.close();
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return theUID;
	}
	
	
	public void DoUpdates(String passedSQL)
	{
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       
        
        
       PreparedStatement ps=conn.prepareStatement(passedSQL);
       ps.executeUpdate();
       ps.close();
       conn.close();
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
	}
	
	public int DoFetch(String passedSQL)
	{
		
		int mycount=0;
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
     	   mycount= rs.getInt(1);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return mycount;
	}
	
	public String DoFetchLoginNmae(String passedSQL)
	{
		
		String mynameIS="";
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   mynameIS= rs.getString("fname")+"LEON"+rs.getString("lname");
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return mynameIS;
	}
	
	
	public int DoReturnInt(String passedSQL,String tablename)
	{
		
		int RetunInt=0;
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   RetunInt= rs.getInt(tablename);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return RetunInt;
	}
	
	public String DoReturnString(String passedSQL,String tablename)
	{
		
		String RetunString="";
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   RetunString= rs.getString(tablename);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return RetunString;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public IndexedContainer DoFetchRtable(String passedSQL,final IndexedContainer passTable,final Table admintable2,final String myview)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   HorizontalLayout UserDefiner = new HorizontalLayout();
    		  UserDefiner.setSpacing(true);
    	 final  Button Editbutton = new Button("Edit");
			Editbutton.setStyleName("link style");
			Editbutton.setId(rs.getString("UID"));
		
			Editbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					formEditor("SELECT * FROM User_entity WHERE UID="+Integer.parseInt(Editbutton.getId())+"",myview);
					
				}
			});
			final  Button Deletebutton = new Button("Delete");
			 Deletebutton.setStyleName("link style");
			 Deletebutton.setId(rs.getString("UID"));
			
			Deletebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
				
					UI.getCurrent().addWindow(MyAlert("DELETE FROM User_entity WHERE UID="+Integer.parseInt(Deletebutton.getId())+"",admintable2,myview));
					//DeleteRecord("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"");
					
				
					
					
				}
			});
			
			UserDefiner.addComponent(Editbutton);
			UserDefiner.addComponent(Deletebutton);
    	 
    	   
    		 passTable.addItem(rs.getInt("UID"));
			 passTable.getContainerProperty(rs.getInt("UID"), "First Name").setValue( rs.getString("fname"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Last Name").setValue(rs.getString("lname"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Email").setValue(rs.getString("email"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("created_timestamp"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Action").setValue(UserDefiner);
       }
       stmt.close();
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	@SuppressWarnings("unchecked")
	public IndexedContainer DoFetchRtableEmail(String passedSQL,final IndexedContainer passTable,final Table admintable2,final String myview)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   HorizontalLayout UserDefiner = new HorizontalLayout();
    		  UserDefiner.setSpacing(true);
    	 final  Button Editbutton = new Button("Edit");
			Editbutton.setStyleName("link style");
			Editbutton.setId(rs.getString("UID"));
		
			Editbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					formEmailsetEditor("SELECT * FROM Email_settings WHERE UID="+Integer.parseInt(Editbutton.getId())+"",myview);
					
				}
			});
			final  Button Deletebutton = new Button("Delete");
			 Deletebutton.setStyleName("link style");
			 Deletebutton.setId(rs.getString("UID"));
			
			Deletebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
				
					UI.getCurrent().addWindow(MyAlert("DELETE FROM Email_settings WHERE UID="+Integer.parseInt(Deletebutton.getId())+"",admintable2,myview));
					//DeleteRecord("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"");
					
				
					
					
				}
			});
			
			UserDefiner.addComponent(Editbutton);
			UserDefiner.addComponent(Deletebutton);
    	 
    	   
    		 passTable.addItem(rs.getInt("UID"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Host").setValue( rs.getString("host_adress"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Sender Email").setValue(rs.getString("sender_email"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("created_timestamp"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Action").setValue(UserDefiner);
       }
       stmt.close();
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	@SuppressWarnings("unchecked")
	public IndexedContainer AcountDoFetchRtable(String passedSQL,final IndexedContainer passTable,final Table admintable2,final String myview)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   HorizontalLayout UserDefiner = new HorizontalLayout();
    		  UserDefiner.setSpacing(true);
    	 final  Button Editbutton = new Button("Edit");
			Editbutton.setStyleName("link style");
			Editbutton.setId(rs.getString("UID"));
		
			Editbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					formEditor("SELECT * FROM User_entity WHERE UID="+Integer.parseInt(Editbutton.getId())+"",myview);
					
				}
			});
			final  Button Deletebutton = new Button("Delete");
			 Deletebutton.setStyleName("link style");
			 Deletebutton.setId(rs.getString("UID"));
			
			Deletebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
				
					UI.getCurrent().addWindow(MyAlert("DELETE FROM User_entity WHERE UID="+Integer.parseInt(Deletebutton.getId())+"",admintable2,myview));
					//DeleteRecord("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"");
					
				
					
					
				}
			});
			
			UserDefiner.addComponent(Editbutton);
			//UserDefiner.addComponent(Deletebutton);
    	 
    	   
    		 passTable.addItem(rs.getInt("UID"));
			 passTable.getContainerProperty(rs.getInt("UID"), "First Name").setValue( rs.getString("fname"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Last Name").setValue(rs.getString("lname"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Email").setValue(rs.getString("email"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("created_timestamp"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Action").setValue(UserDefiner);
       }
       stmt.close();
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public IndexedContainer DoFetchStoretable(String passedSQL,IndexedContainer passTable,final Table storetable2)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   HorizontalLayout UserDefiner = new HorizontalLayout();
  		  UserDefiner.setSpacing(true);
  		  
    	 final  Button Editbutton = new Button("Edit");
			Editbutton.setStyleName("link style");
			Editbutton.setId(rs.getString("UID"));
		
			Editbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					formStoreEditor("SELECT * FROM Stores WHERE UID="+Integer.parseInt(Editbutton.getId())+"",storetable2);
				}
			});
			
			final  Button Deletebutton = new Button("Delete");
			 Deletebutton.setStyleName("link style");
			 Deletebutton.setId(rs.getString("UID"));
			
			Deletebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
				
					UI.getCurrent().addWindow(MyAlert("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"",storetable2,"stores"));
					//DeleteRecord("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"");
					
						
					//AboutKwani.updateMytabel(electionCattable);
				
					
					
				}
			});
			UserDefiner.addComponent(Editbutton);
			UserDefiner.addComponent(Deletebutton);
    	  // passTable.addItem(new Object[] {
    			  // rs.getString("name"),rs.getString("email"),rs.getString("phone"), rs.getString("address"),rs.getString("created_timestamp"),
    			  // Editbutton},rs.getInt("UID"));
    	   
    	   passTable.addItem(rs.getInt("UID"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Store Name").setValue(rs.getString("name"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Email").setValue(rs.getString("email"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Phone").setValue(rs.getString("phone"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Address").setValue(rs.getString("address"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("created_timestamp"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Action").setValue(UserDefiner);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	public void formEditor(String passedSQL,String myview)
	{

		String fname="";
		String lname="";
		String email="";
		int UserID=0;
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   fname=rs.getString("fname");
    	   lname=rs.getString("lname");
    	   email=rs.getString("email");
    	   UserID=rs.getInt("UID");
       }
       
       
       rs.close();
       conn.close();
       if(myview.equals("account")){
    	   UI.getCurrent().addWindow(new MyaccountView().EditAminUser(fname,lname,email,UserID));  
       }
       else{
    	   UI.getCurrent().addWindow(new AdminUsers().EditAminUser(fname,lname,email,UserID));
       }
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
	
	
	}
	
	public void formEmailsetEditor(String passedSQL,String myview)
	{

		String fname="";
		String lname="";
		String email="";
		String pass="";
		int UserID=0;
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   fname=rs.getString("host_adress");
    	   lname=rs.getString("sender_email");
    	   pass=rs.getString("host_password");
    	   UserID=rs.getInt("UID");
       }
       
       
       rs.close();
       conn.close();
     
    	   UI.getCurrent().addWindow(new Emailsettings().EditAminUser(fname,lname,UserID,pass));
      
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
	
	
	}
	public void DeleteRecord(String passedSQL)
	{
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
     
      
       
       PreparedStatement ps=conn.prepareStatement(passedSQL,PreparedStatement.RETURN_GENERATED_KEYS);
       ps.executeUpdate();
       ps.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		
	}
	
	
	public void formStoreEditor(String passedSQL, Table storetable2)
	{

		String name="";
		String email="";
		String phone="";
		String address="";
		int StoreID=0;
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   name=rs.getString("name");
    	   email=rs.getString("email");
    	   phone=rs.getString("phone");
    	   address=rs.getString("address");
    	   StoreID=rs.getInt("UID");
       }
       
       
       rs.close();
       conn.close();
       
       UI.getCurrent().addWindow(new StoresView().EditStore(name,email,phone,address,StoreID,storetable2));
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
	
	
	}
	
	public Window MyAlert(final String Querry, final Table storetable2,final String Myview)
	{
		 VerticalLayout l = new VerticalLayout();
         l.setWidth("400px");
         l.setMargin(true);
         l.setSpacing(true);
         final Window alert = new Window("Delete Records", l);
         alert.setModal(true);
         alert.setResizable(false);
         alert.setDraggable(false);
         alert.addStyleName("dialog");
         alert.setClosable(false);

         Label message = new Label(
                 "You are about to delete this record. Do you want to proceed?");
         l.addComponent(message);

         HorizontalLayout buttons = new HorizontalLayout();
         buttons.setWidth("100%");
         buttons.setSpacing(true);
         l.addComponent(buttons);

    

         Button cancel = new Button("Cancel");
         cancel.addStyleName("small");
         cancel.addClickListener(new ClickListener() {
             @Override
             public void buttonClick(ClickEvent event) {
                 alert.close();
             }
         });
         buttons.addComponent(cancel);

         Button ok = new Button("Delete");
         ok.addStyleName("default");
         ok.addStyleName("small");
         ok.addStyleName("wide");
         ok.addClickListener(new ClickListener() {
             @Override
             public void buttonClick(ClickEvent event) {
                 //editors.removeComponent(tabContent);
               //  draftCount--;
               //  ((DashboardUI) UI.getCurrent())
                //         .updateReportsButtonBadge(draftCount + "");
            	 
            		DeleteRecord(Querry);
					
                 alert.close();
                 Notification
                         .show("The record has been deleted",
                                 "Actually, it will not show in future records.",
                                 Type.TRAY_NOTIFICATION);
                 
                 if(Myview.equalsIgnoreCase("stores")){
                	 new StoresView().updateTable(storetable2);
                 }
                 else if(Myview.equalsIgnoreCase("storesbranch")){
                	 new StoreBranchView().updateTable(storetable2);
                 }
                 else if(Myview.equalsIgnoreCase("terminal")){
                	 new TerminalView().updateTable(storetable2);
                 }
                 else if(Myview.equalsIgnoreCase("admin")){
                	 new AdminUsers().updateTable(storetable2);
                 }
                 else if(Myview.equalsIgnoreCase("emailset")){
                	 new Emailsettings().updateTable(storetable2);
                 }
                 

             }
         });
         buttons.addComponent(ok);
         ok.focus();

         alert.addShortcutListener(new ShortcutListener("Cancel",
                 KeyCode.ESCAPE, null) {
             @Override
             public void handleAction(Object sender, Object target) {
                 alert.close();
             }
         });
         
         return alert;
	}
}
