package com.example.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.example.vtest.StoresView;
import com.example.vtest.TerminalView;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;

public class TerminalModel 
{
	   Navigator navigator;
	
	@SuppressWarnings("unchecked")
	public IndexedContainer DoFetchTerminaltable(String passedSQL,IndexedContainer passTable,final Table storetable2)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   HorizontalLayout UserDefiner = new HorizontalLayout();
   		  UserDefiner.setSpacing(true);
    	 final  Button Editbutton = new Button("Edit");
			Editbutton.setStyleName("link style");
			Editbutton.setId(rs.getString("UID"));
		
			Editbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					formTerminalEditor("SELECT * FROM Terminals WHERE UID="+Integer.parseInt(Editbutton.getId())+"",storetable2);
				}
			});
			
			final  Button Deletebutton = new Button("Delete");
			 Deletebutton.setStyleName("link style");
			 Deletebutton.setId(rs.getString("UID"));
			
			Deletebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
				
					UI.getCurrent().addWindow(new logins().MyAlert("DELETE FROM Terminals WHERE UID="+Integer.parseInt(Deletebutton.getId())+"",storetable2,"terminal"));
					//DeleteRecord("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"");
					
						
					//AboutKwani.updateMytabel(electionCattable);
				
					
					
				}
			});
			UserDefiner.addComponent(Editbutton);
			UserDefiner.addComponent(Deletebutton);
			
			 passTable.addItem(rs.getInt("UID"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Imei No").setValue(rs.getString("IMEI_no"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Serial No").setValue(rs.getString("Serial_no"));
			 passTable.getContainerProperty(rs.getInt("UID"), "App Id").setValue(rs.getString("app_id"));
			 passTable.getContainerProperty(rs.getInt("UID"), "App Version").setValue(rs.getString("app_version"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Phone").setValue(rs.getString("phone"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Type").setValue(rs.getString("type"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Min Amount").setValue(rs.getDouble("min_amount"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Max Amount").setValue(rs.getDouble("max_amount"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Store").setValue(DoFetchStoreName(rs.getInt("Store_branches_fk"),"name"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Branch").setValue(DoFetchBranch(rs.getInt("Store_branches_fk"),"name"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("created_timestamp"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Status").setValue(rs.getString("status"));
			 passTable.getContainerProperty(rs.getInt("UID"), "Action").setValue(UserDefiner);
			 
    
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	public ComboBox DoFetchStorelist(String passedSQL,ComboBox passSelect)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   passSelect.addItem(rs.getString("name"));
    	  
    	 }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passSelect;
	}
	
	public ComboBox DoFetchBranchlist(String passedSQL,ComboBox passSelect)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   passSelect.addItem(rs.getString("name")+"("+DoFetchStoreName(rs.getInt("UID"),"name")+")");
    	 }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passSelect;
	}
	
	public String getTStatus(int DbStatus)
	{
		String Tstatus="";
		
		if(DbStatus==0)
		{
			Tstatus="idle";
		}
		
		else if(DbStatus==1)
		{
			Tstatus="suspended";
		}
		
		else if(DbStatus==2)
		{
			Tstatus="broken";
		}
		
		else if(DbStatus==3)
		{
			Tstatus="running";
		}
		
		
		
		
		return Tstatus;
		
	}
	
	public void formTerminalEditor(String passedSQL,final Table storetable2)
	{

		String EImei="";
		String Eserial="";
		String EappId="";
		String EappVersion="";
		String Ephone="";
		String Eminamount="";
		String Emaxamount="";
		int terminalD=0;
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   EImei=rs.getString("IMEI_no");
    	   Eserial=rs.getString("Serial_no");
    	   EappId=rs.getString("app_id");
    	   EappVersion=rs.getString("app_version");
    	   Ephone=rs.getString("phone");
    	   Eminamount=rs.getString("min_amount");
    	   Emaxamount=rs.getString("max_amount");
    	   terminalD=rs.getInt("UID");
       }
       
       
       rs.close();
       conn.close();
       
       UI.getCurrent().addWindow(new TerminalView().EditTerminal(EImei,Eserial,EappId,EappVersion,Ephone,terminalD,storetable2,Eminamount,Emaxamount));
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
	
	
	}
	
	public String DoFetchStoreName(int theId,String tablename)
	{
		
		String theName="not deployed";
		int theStoreID=0;
		
		if(theId>0){
			
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery("SELECT * FROM Store_branches WHERE UID="+theId+"");
      
       
       
       while(rs.next())
       {
    	   theStoreID= rs.getInt("Store_fk");
       }
       rs.close();
       
       
      ResultSet rs1=stmt.executeQuery("SELECT * FROM Stores WHERE UID="+theStoreID+"");
      
       
       
       while(rs1.next())
       {
    	   theName= rs1.getString(tablename);
       }
       rs1.close();
       
       
       
       
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		}
		return theName;
	}
	
	
	
	public String DoFetchStoreBranchIds(String storename)
	{
		
		String theIDS="";
		int theStoreID=0;
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
      // ResultSet rs=stmt.executeQuery("SELECT * FROM Store_branches WHERE UID="+theId+"");
       ResultSet rs=stmt.executeQuery("SELECT * FROM Stores WHERE name='"+storename+"'");
      
       
       
       while(rs.next())
       {
    	   theStoreID= rs.getInt("UID");
       }
       rs.close();
       
       
      ResultSet rs1=stmt.executeQuery("SELECT GROUP_CONCAT(UID) FROM Store_branches WHERE Store_fk="+theStoreID+"");
      
       
       
       while(rs1.next())
       {
    	   theIDS= rs1.getString(1);
       }
       rs1.close();
       
       
       
       
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return theIDS;
	}
	
	
	public String DoFetchBranch(int theId,String tablename)
	{
		
		String theName="not deployed";
		
		if(theId>0){
			
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery("SELECT * FROM Store_branches WHERE UID="+theId+"");
      
       
       
       while(rs.next())
       {
    	   theName= rs.getString(tablename);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		}
		return theName;
	}
	
	
	

}
