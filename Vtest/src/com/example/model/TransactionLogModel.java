package com.example.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.Button.ClickEvent;

public class TransactionLogModel 
{

	
	
	
	
	@SuppressWarnings("unchecked")
	public Container DoFetchBranchtable(String passedSQL,Container passTable)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	
    	   //passTable.addItem(new Object[] {
    			   //rs.getString("PAN"),rs.getString("STAN"),DoFetchStoreName(rs.getInt("Store_branches_fk"),"name"),
    			  // DoFetchBranch(rs.getInt("Store_branches_fk"),"name"),rs.getDouble("Trans_Amount"),
    			  // rs.getString("MTI"),rs.getString("Transmission_date"),rs.getString("Response_code")});
    	   passTable.addItem(rs.getInt("UID"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Account No").setValue(rs.getString("PAN"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "TCode").setValue(rs.getString("STAN"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Store").setValue(DoFetchStoreName(rs.getInt("Store_branches_fk"),"name"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Store branch").setValue(DoFetchBranch(rs.getInt("Store_branches_fk"),"name"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Amount").setValue(rs.getDouble("Trans_Amount"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Type").setValue(rs.getString("MTI"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("Transmission_date"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Status").setValue(rs.getString("Response_code"));
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	public String DoFetchBranch(int theId,String tablename)
	{
		
		String theName="not deployed";
		
		if(theId>0){
			
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery("SELECT * FROM Store_branches WHERE UID="+theId+"");
      
       
       
       while(rs.next())
       {
    	   theName= rs.getString(tablename);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		}
		return theName;
	}
	
	public String DoFetchStoreName(int theId,String tablename)
	{
		
		String theName="not deployed";
		int theStoreID=0;
		
		if(theId>0){
			
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery("SELECT * FROM Store_branches WHERE UID="+theId+"");
      
       
       
       while(rs.next())
       {
    	   theStoreID= rs.getInt("Store_fk");
       }
       rs.close();
       
       
      ResultSet rs1=stmt.executeQuery("SELECT * FROM Stores WHERE UID="+theStoreID+"");
      
       
       
       while(rs1.next())
       {
    	   theName= rs1.getString(tablename);
       }
       rs1.close();
       
       
       
       
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		}
		return theName;
	}
}
