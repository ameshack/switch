package com.example.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.example.vtest.AdminUsers;
import com.example.vtest.StoreBranchView;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;

public class StoreBranchModel
{
	 Navigator navigator;
	
	@SuppressWarnings("unchecked")
	public IndexedContainer DoFetchBranchtable(String passedSQL,IndexedContainer passTable,final Table storetable2)
	{
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	  HorizontalLayout UserDefiner = new HorizontalLayout();
   		  UserDefiner.setSpacing(true);
    	   
    	 final  Button Editbutton = new Button("Edit");
			Editbutton.setStyleName("link style");
			Editbutton.setId(rs.getString("UID"));
		
			Editbutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					formBranchEditor("SELECT * FROM Store_branches WHERE UID="+Integer.parseInt(Editbutton.getId())+"",storetable2);
				}
			});
			
			final  Button Deletebutton = new Button("Delete");
			 Deletebutton.setStyleName("link style");
			 Deletebutton.setId(rs.getString("UID"));
			
			Deletebutton.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
				
					UI.getCurrent().addWindow(new logins().MyAlert("DELETE FROM Store_branches WHERE UID="+Integer.parseInt(Deletebutton.getId())+"",storetable2,"storesbranch"));
					//DeleteRecord("DELETE FROM Stores WHERE UID="+Integer.parseInt(Deletebutton.getId())+"");
					
				
					
					
				}
			});
			UserDefiner.addComponent(Editbutton);
			UserDefiner.addComponent(Deletebutton);
			
    	  // passTable.addItem(new Object[] {
    		//	   rs.getString("name"),rs.getString("email"),rs.getString("phone"),
    		//	   DoFetchBranchName(rs.getInt("Store_fk"),"name"),rs.getString("created_timestamp"),Editbutton},rs.getInt("UID"));
    	   
      	   passTable.addItem(rs.getInt("UID"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Name").setValue(rs.getString("name"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Email").setValue(rs.getString("email"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Phone").setValue(rs.getString("phone"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Store").setValue(DoFetchBranchName(rs.getInt("Store_fk"),"name"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Date").setValue(rs.getString("created_timestamp"));
    	   passTable.getContainerProperty(rs.getInt("UID"), "Action").setValue(UserDefiner);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
		return passTable;
	}
	
	public String DoFetchBranchName(int theId,String tablename)
	{
		
		String theName="no store";
		
		if(theId>0){
			
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
       Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery("SELECT * FROM Stores WHERE UID="+theId+"");
      
       
       
       while(rs.next())
       {
    	   theName= rs.getString(tablename);
       }
       rs.close();
       conn.close();
       
       
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		}
		return theName;
	}
	
	public void formBranchEditor(String passedSQL,Table storetable2)
	{

		String name="";
		String phone="";
		String email="";
		int StoreID=0;
		int branchID=0;
		
		
		try{
			
	   Class.forName(dbsettings.dbDriver);
 
      final Connection conn=DriverManager.getConnection(dbsettings.dbUrl,dbsettings.dbUsername,dbsettings.dbPassword);
       final Statement stmt=conn.createStatement();
       
       ResultSet rs=stmt.executeQuery(passedSQL.trim());
      
       
       
       while(rs.next())
       {
    	   name=rs.getString("name");
    	   email=rs.getString("email");
    	   phone=rs.getString("phone");
    	   StoreID=rs.getInt("Store_fk");
    	   branchID=rs.getInt("UID");
       }
       
       
       rs.close();
       conn.close();
       
       UI.getCurrent().addWindow(new StoreBranchView().EditStoreBranch(name,email,phone,StoreID,branchID,storetable2));
       
		}
       catch(ClassNotFoundException ex)
       {
           ex.printStackTrace();
           System.out.println("Could not load the Drivers");
       }
       catch(SQLException ex)
       {
           ex.printStackTrace();
           System.out.println(ex.getMessage());
       }
		
	
	
	}
	
	
	
}
